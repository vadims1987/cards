﻿using Microsoft.AspNetCore.Mvc;

namespace Cards.Manager.Controllers
{
    [Route("home")]
    public class HomeController
    {
        [Route("index")]
        public string Index()
        {
            return "Hello, World! this message is from Home Controller...";
        }
    }
}
