﻿using Cards.Manager.Models;
using Microsoft.AspNetCore.Mvc;

namespace Cards.Manager.Controllers
{
    [Route("about")]
    public class AboutController : Controller
    {
        [Route("phone")]
        public string Phone()
        {
            return "+49-333-3333333";
        }

        [Route("country")]
        public string Country()
        {
            return "Germany";
        }

        [Route("player")]
        public ObjectResult Player()
        {
            var player = new Player { Id = 1, Name = "Mark Upston" };
            return new ObjectResult(player);
        }
    }
}
