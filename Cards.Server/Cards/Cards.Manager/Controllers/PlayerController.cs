﻿using Cards.Manager.Models;
using Microsoft.AspNetCore.Mvc;

namespace Cards.Manager.Controllers
{
    [Route("player")]
    public class PlayerController : Controller
    {
        private readonly CardsContext _context;

        public PlayerController(CardsContext context)
        {
            _context = context;
        }

        public ObjectResult GetAll()
        {
            var repository = new PlayerRepository(_context);
            return new ObjectResult(repository.GetAll());
        }
    }
}
