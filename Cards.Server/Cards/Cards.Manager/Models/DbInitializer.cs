﻿using System.Linq;

namespace Cards.Manager.Models
{
    public static class DbInitializer
    {
        public static void Initialize(CardsContext context)
        {
            if (context.Players.Any())
            {
                return;   
            }

            var players = new[]
            {
                new Player
                {
                    Name = "Vadim"
                }
            };

            foreach (var player in players)
            {
                context.Players.Add(player);
            }

            context.SaveChanges();
        }
    }
}
