﻿using Microsoft.EntityFrameworkCore;

namespace Cards.Manager.Models
{
    public class CardsContext : DbContext
    {
        public CardsContext(DbContextOptions<CardsContext> options) : base(options)
        {
        }

        public DbSet<Player> Players { get; set; }
    }
}
