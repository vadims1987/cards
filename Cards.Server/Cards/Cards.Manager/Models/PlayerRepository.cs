﻿using System.Collections.Generic;
using System.Linq;

namespace Cards.Manager.Models
{
    public class PlayerRepository
    {
        private CardsContext Context { get; set; }

        public PlayerRepository(CardsContext context)
        {
            Context = context;
        }
        
        public IEnumerable<Player> GetAll()
        {
            return Context.Players.ToList();
        }
    }
}
