﻿using Moq;
using NUnit.Framework;

namespace Assets.Scripts.Editor
{
    public abstract class GridLegalHandTestBase : TestFixtureBase
    {
        [SetUp]
        public void CommonInstall()
        {
            Container.Bind<ILevelSettings>().To<LevelSettings>().AsSingle();
            Container.Bind<ILegalHandsFinder>().To<LegalHandsFinder>().AsTransient();
            Container.Bind<ICandidateLegalHandsExtractor>().To<CandidateLegalHandsExtractor>().AsTransient();
            Container.Bind<IIntersectedLegalHandsFilter>().To<IntersectedLegalHandsFilter>().AsTransient();

            Container.Inject(this);
        }

        protected Mock<IGrid> CreateGridMock(Card[,] cards)
        {
            var levelSettings = Container.Resolve<ILevelSettings>();
            levelSettings.NumberOfColumns = cards.GetLength(0);
            levelSettings.NumberOfRows = cards.GetLength(1);

            var gridMock = new Mock<IGrid>();

            for (var i = 0; i < cards.GetLength(0); i++)
            {
                for (var j = 0; j < cards.GetLength(1); j++)
                {
                    var gameCardMock = new Mock<IGameCard>();

                    gameCardMock.Setup(gameCard => gameCard.Card).Returns(cards[i, j]);
                    gridMock.Setup(grid => grid[new PositionOnBoard(i, j)]).Returns(gameCardMock.Object as IGameEntity);
                }
            }

            return gridMock;
        }
    }
}
