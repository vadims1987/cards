﻿using Moq;
using NUnit.Framework;

namespace Assets.Scripts.Editor
{
    public class StraightGridLegalHandTest : GridLegalHandTestBase
    {
        [Test]
        public void StraightLegalHand_HasStraight_Success()
        {
            var levelSettings = Container.Resolve<ILevelSettings>();
            levelSettings.NumberOfColumns = 5;
            levelSettings.NumberOfRows = 2;

            var legalHandsFinder = Container.Resolve<ILegalHandsFinder>();

            var gridMock = CreateGridMock();
            var foundLegalHands = legalHandsFinder.FindLegalHands(gridMock.Object);

            Assert.That(foundLegalHands.Count, Is.EqualTo(2));
        }

        [Test]
        public void StraightLegalHand_HasStraight_Failure()
        {
            var levelSettings = Container.Resolve<ILevelSettings>();
            levelSettings.NumberOfColumns = 5;
            levelSettings.NumberOfRows = 2;

            var legalHandsFinder = Container.Resolve<ILegalHandsFinder>();

            var gridMock = CreateGridMock();
            var foundLegalHands = legalHandsFinder.FindLegalHands(gridMock.Object);

            Assert.IsFalse(foundLegalHands.Count == 1);
        }

        private Mock<IGrid> CreateGridMock()
        {
            var cards = new[,]
            {
                {
                    new Card(Suit.Clubs, Rank.Two), new Card(Suit.Diamonds, Rank.Three), new Card(Suit.Hearts, Rank.Four),
                    new Card(Suit.Clubs, Rank.Five), new Card(Suit.Spades, Rank.Six)
                },
                {
                    new Card(Suit.Diamonds, Rank.Five), new Card(Suit.Hearts, Rank.Six),
                    new Card(Suit.Diamonds, Rank.Seven), new Card(Suit.Clubs, Rank.Eight),
                    new Card(Suit.Spades, Rank.Nine)
                }
            };

            return CreateGridMock(cards);
        }
    }
}