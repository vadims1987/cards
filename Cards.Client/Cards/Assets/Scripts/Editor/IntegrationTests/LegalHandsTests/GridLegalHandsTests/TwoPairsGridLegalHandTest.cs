﻿using Moq;
using NUnit.Framework;

namespace Assets.Scripts.Editor
{
    public class TwoPairsGridLegalHandTest : GridLegalHandTestBase
    {
        [Test]
        public void TwoPairsLegalHand_HasPairs_Success()
        {
            var levelSettings = Container.Resolve<ILevelSettings>();
            levelSettings.NumberOfColumns = 5;
            levelSettings.NumberOfRows = 2;

            var legalHandsFinder = Container.Resolve<ILegalHandsFinder>();

            var gridMock = CreateGridMock();
            var foundLegalHands = legalHandsFinder.FindLegalHands(gridMock.Object);

            Assert.That(foundLegalHands.Count, Is.EqualTo(2));
        }

        [Test]
        public void TwoPairsLegalHand_HasPairs_Failure()
        {
            var levelSettings = Container.Resolve<ILevelSettings>();
            levelSettings.NumberOfColumns = 4;
            levelSettings.NumberOfRows = 2;

            var legalHandsFinder = Container.Resolve<ILegalHandsFinder>();

            var gridMock = CreateGridMock();
            var foundLegalHands = legalHandsFinder.FindLegalHands(gridMock.Object);

            Assert.IsFalse(foundLegalHands.Count == 1);
        }

        private Mock<IGrid> CreateGridMock()
        {
            var cards = new[,]
            {
                {
                    new Card(Suit.Clubs, Rank.Ace), new Card(Suit.Diamonds, Rank.Ace), new Card(Suit.Hearts, Rank.King),
                    new Card(Suit.Clubs, Rank.King), new Card(Suit.Clubs, Rank.Two)
                },
                {
                    new Card(Suit.Diamonds, Rank.Three), new Card(Suit.Hearts, Rank.Three),
                    new Card(Suit.Diamonds, Rank.Six), new Card(Suit.Hearts, Rank.Seven),
                    new Card(Suit.Clubs, Rank.Queen)
                }
            };

            return CreateGridMock(cards);
        }
    }
}