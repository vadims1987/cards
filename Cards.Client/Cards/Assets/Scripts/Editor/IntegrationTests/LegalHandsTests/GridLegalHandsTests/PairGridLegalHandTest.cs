﻿using Moq;
using NUnit.Framework;

namespace Assets.Scripts.Editor
{
    public class PairGridLegalHandTest : GridLegalHandTestBase
    {
        [Test]
        public void PairLegalHand_HasPairs_Success()
        {
            var legalHandsFinder = Container.Resolve<ILegalHandsFinder>();

            var gridMock = CreateGridMock();
            var foundLegalHands = legalHandsFinder.FindLegalHands(gridMock.Object);

            Assert.That(foundLegalHands.Count, Is.EqualTo(2));
        }

        [Test]
        public void PairLegalHand_HasPairs_Failure()
        {
            var legalHandsFinder = Container.Resolve<ILegalHandsFinder>();

            var gridMock = CreateGridMock();
            var foundLegalHands = legalHandsFinder.FindLegalHands(gridMock.Object);

            Assert.That(foundLegalHands.Count, Is.EqualTo(1));
        }

        private Mock<IGrid> CreateGridMock()
        {
            var cards = new[,]
            {
                {new Card(Suit.Clubs, Rank.Ace), new Card(Suit.Diamonds, Rank.Ace)},
                {new Card(Suit.Clubs, Rank.King), new Card(Suit.Diamonds, Rank.King)}
            };

            return CreateGridMock(cards);
        }
    }
}