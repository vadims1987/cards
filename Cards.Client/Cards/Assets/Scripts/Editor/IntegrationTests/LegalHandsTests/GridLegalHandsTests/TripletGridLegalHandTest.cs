﻿using Moq;
using NUnit.Framework;

namespace Assets.Scripts.Editor
{
    public class TripletGridLegalHandTest : GridLegalHandTestBase
    {
        [Test]
        public void TripletLegalHand_HasTriplet_Success()
        {
            var levelSettings = Container.Resolve<ILevelSettings>();
            levelSettings.NumberOfColumns = 3;
            levelSettings.NumberOfRows = 2;

            var legalHandsFinder = Container.Resolve<ILegalHandsFinder>();

            var gridMock = CreateGridMock();
            var foundLegalHands = legalHandsFinder.FindLegalHands(gridMock.Object);

            Assert.That(foundLegalHands.Count, Is.EqualTo(2));
        }

        [Test]
        public void TripleLegalHand_HasTriplet_Failure()
        {
            var levelSettings = Container.Resolve<ILevelSettings>();
            levelSettings.NumberOfColumns = 3;
            levelSettings.NumberOfRows = 2;

            var legalHandsFinder = Container.Resolve<ILegalHandsFinder>();

            var gridMock = CreateGridMock();
            var foundLegalHands = legalHandsFinder.FindLegalHands(gridMock.Object);

            Assert.IsFalse(foundLegalHands.Count == 1);
        }

        private Mock<IGrid> CreateGridMock()
        {
            var cards = new[,]
            {
                {new Card(Suit.Clubs, Rank.Ace), new Card(Suit.Diamonds, Rank.Ace), new Card(Suit.Hearts, Rank.Ace)},
                {new Card(Suit.Clubs, Rank.King), new Card(Suit.Diamonds, Rank.King), new Card(Suit.Hearts, Rank.King)}
            };

            return CreateGridMock(cards);
        }
    }
}