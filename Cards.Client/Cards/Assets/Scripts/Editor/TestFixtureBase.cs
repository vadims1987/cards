﻿using NUnit.Framework;
using Zenject;

namespace Assets.Scripts.Editor
{
    public abstract class TestFixtureBase
    {
        protected DiContainer Container { get; private set; }

        [SetUp]
        public virtual void Setup()
        {
            Container = new DiContainer();
        }
    }
}
