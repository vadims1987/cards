﻿using NUnit.Framework;

namespace Assets.Scripts.Editor
{
    public class NoneExtractorTests
    {
        [Test]
        public void NoneExtractorTest_AnyCards_Success()
        {
            var extractor = new NoneExtractor();
            var foundLegalHand = extractor.GetLegalHand(null);

            Assert.That(foundLegalHand.LegalHand, Is.EqualTo(LegalHand.None));
            Assert.That(foundLegalHand.LegalHandOnBoard, Is.EqualTo(null));
        }
    }
}
