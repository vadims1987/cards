﻿using NUnit.Framework;

namespace Assets.Scripts.Editor
{
    public class IntersectedLegalHandsFilterTests
    {
        [Test]
        public void IntersectedLegalHandsFilterTest_AnyCards_Success()
        {
            var intersectionFilter = new IntersectedLegalHandsFilter();
            var foundLegalHand = intersectionFilter.FilterIntersectedLegalHands(null);

            Assert.That(foundLegalHand.Count, Is.EqualTo(0));
        }
    }
}
