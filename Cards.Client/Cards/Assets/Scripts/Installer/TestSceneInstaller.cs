﻿
using Assets.Scripts.Test;

namespace Assets.Scripts.Installer
{
    public class TestSceneInstaller : SceneInstallerBase
    {
        public int LevelNumber;

        public override void InstallBindings()
        {
            base.InstallBindings();
            Container.Bind<ICardResolver>().To<TestCardResolver>();

            InstallPersistanceBindings();
            InstallLevelMapBindings<TestObjectivesMap>();
            InstallLevelHandlerBindings<TestSettingsHandler>();

            SetupLevelObjectivesMaps();
            SetupLevelSettingsMaps();

            SetLevelSettings();
        }

        private void SetLevelSettings()
        {
            var levelSettings = Container.Resolve<ILevelSettingsHandler>().LevelSettings;
            Container.Bind<ILevelSettings>().FromInstance(levelSettings).AsSingle();
        }
    }
}
