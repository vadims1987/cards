﻿namespace Assets.Scripts
{
    public class MainSceneInstaller : SceneInstallerBase
    {
        public static bool AreMapsInitialized;

        public override void InstallBindings()
        {
            base.InstallBindings();
            Container.Bind<ILevelLoader>().To<LevelLoader>().AsSingle();

            InstallPersistanceBindings();
            InstallLevelMapBindings<LevelObjectivesMap>();
            InstallLevelHandlerBindings<LevelSettingsHandler>();

            SetupLevelObjectivesMaps();
            SetupLevelSettingsMaps();

            AreMapsInitialized = true;
        }
    }
}
