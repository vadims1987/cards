﻿
namespace Assets.Scripts.Installer
{
    public class LevelSceneInstaller : SceneInstallerBase
    {
        public override void InstallBindings()
        {
            base.InstallBindings();
            Container.Bind<ICardResolver>().To<CardResolver>().AsSingle();
            Container.Bind<ILevelLoader>().To<LevelLoader>().AsSingle();

            InstallLevelMapBindings<LevelObjectivesMap>();
            InstallLevelHandlerBindings<LevelSettingsHandler>();

            if (!MainSceneInstaller.AreMapsInitialized)
            {
                LoadLevelSettings();
            }

            SetLevelSettings();
        }

        private void LoadLevelSettings()
        {
            InstallPersistanceBindings();

            SetupLevelObjectivesMaps();
            SetupLevelSettingsMaps();
        }

        private void SetLevelSettings()
        {
            var levelSettings = Container.Resolve<ILevelSettingsHandler>().LevelSettings;
            Container.Bind<ILevelSettings>().FromInstance(levelSettings).AsSingle();
        }
    }
}
