﻿using System.Collections.Generic;
using Assets.Scripts.Persistence;
using Zenject;

namespace Assets.Scripts
{
    public abstract class SceneInstallerBase : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<ISwipeDetector>().To<SwipeDetector>().AsSingle();
            Container.Bind<IGrid>().To<Grid>().AsSingle();
            Container.Bind<IGridFiller>().To<GridFiller>().AsTransient();
            Container.Bind<ILegalHandsFinder>().To<LegalHandsFinder>().AsTransient();
            Container.Bind<ICandidateLegalHandsExtractor>().To<CandidateLegalHandsExtractor>().AsTransient();
            Container.Bind<IIntersectedLegalHandsFilter>().To<IntersectedLegalHandsFilter>().AsTransient();

            Container.Bind<IWorldPositionHelper>().To<WorldPositionHelper>().AsTransient();

            InstallGameManagers();
            InstallSignalBindings();
            InstallEntitiesBindings();
        }

        private void InstallGameManagers()
        {
            Container.Bind<ILevelManager>().To<LevelManager>().AsTransient();
            Container.Bind<ITurnManager>().To<TurnManager>().AsSingle();
            Container.Bind<IObjectivesManager>().To<ObjectivesManager>().AsSingle();
        }

        private void InstallSignalBindings()
        {
            Container.DeclareSignal<CardSwipedSignal>();
            Container.DeclareSignal<TurnUpdatedSignal>();
            Container.DeclareSignal<GameWonSignal>();
            Container.DeclareSignal<GameLostSignal>();
            Container.DeclareSignal<ObjectivesUpdatedSignal>();
        }

        private void InstallEntitiesBindings()
        {
            Container.Bind<IGameEntitySpawner>().To<GameEntitySpawner>().AsTransient();
            Container.BindFactory<GameEntity, GameCard.Factory>().FromComponentInNewPrefab(GameObjects.CardPrefab);
            Container.BindFactory<GameEntity, GameObstacle.Factory>().FromComponentInNewPrefab(GameObjects.ObstaclePrefab);
            Container.BindFactory<GameTile, GameTile.Factory>().FromComponentInNewPrefab(GameObjects.EmptyTilePrefab);
        }

        public void InstallLevelHandlerBindings<T>() where T : ILevelSettingsHandler
        {
            Container.Bind<ILevelSettingsHandler>().To<T>().AsSingle();
        }

        public void InstallLevelMapBindings<T>() where T : ILevelMap<ILevelObjectives>
        {
            Container.Bind<ILevelMap<ILevelObjectives>>().To<T>().AsSingle();
            Container.Bind<ILevelMap<ILevelSettings>>().To<LevelSettingsMap>().AsSingle();
        }

        public void InstallPersistanceBindings()
        {
            Container.Bind<IMapper<SavedLevelObjectivesDTO, IEnumerable<ILevelObjectives>>>().To<LevelObjectivesMapper>().AsTransient();
            Container.Bind<IMapper<SavedLevelSettingsDTO, IEnumerable<ILevelSettings>>>().To<LevelSettingsMapper>().AsTransient();

            Container.Bind<IRepository<IEnumerable<ILevelObjectives>>>().To<LevelObjectivesRepository>().AsTransient();
            Container.Bind<IRepository<IEnumerable<ILevelSettings>>>().To<LevelSettingsRepository>().AsTransient();
        }

        public void SetupLevelSettingsMaps()
        {
            var levelSettingsList = Container.Resolve<IRepository<IEnumerable<ILevelSettings>>>().Load();
            Container.Resolve<ILevelMap<ILevelSettings>>().Setup(levelSettingsList);
        }

        public void SetupLevelObjectivesMaps()
        {
            var levelObjectivesList = Container.Resolve<IRepository<IEnumerable<ILevelObjectives>>>().Load();
            Container.Resolve<ILevelMap<ILevelObjectives>>().Setup(levelObjectivesList);
        }
    }
}
