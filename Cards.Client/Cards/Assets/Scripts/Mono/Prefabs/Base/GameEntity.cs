﻿using UnityEngine;

namespace Assets.Scripts
{
    public abstract class GameEntity : MonoBehaviour, IGameEntity
    {
        protected IEntity Entity;

        public void Init(IEntity entity, Sprite sprite)
        {
            Entity = entity;

            name = Entity.ToString();
            GetComponent<SpriteRenderer>().sprite = sprite;

            transform.SetParent(GameObjects.Grid.transform, false);
        }

        public abstract void UpdatePosition(PositionOnBoard newPosition);

        public abstract void Clear();

        public bool CanMove { get { return Entity.CanMove; } }

        public override string ToString()
        {
            return Entity.ToString();
        }
    }
}
