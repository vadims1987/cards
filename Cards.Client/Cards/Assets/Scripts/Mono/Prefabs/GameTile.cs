﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts
{
    public class GameTile : MonoBehaviour
    {
        [Inject]
        private ISwipeDetector SwipeDetector { get; set; }

        [Inject]
        private IWorldPositionHelper WorldPositionHelper { get; set; }

        private PositionOnBoard _positionOnBoard;

        public PositionOnBoard PositionOnBoard
        {
            get { return _positionOnBoard; }
            set
            {
                _positionOnBoard = value;
                SetTileWorldPosition();
            }
        }

        private void SetTileWorldPosition()
        {
            var gridTilePosition = GridPositionHelper.GetGridPosition(_positionOnBoard);
            var tileWorldPosition = WorldPositionHelper.GetEntityWorldPosition(gridTilePosition, transform.position);
            transform.position = tileWorldPosition;
        }

        private void OnMouseEnter()
        {
            if (!GameLocker.IsLocked)
            {
                SwipeDetector.TileEnterDetected(PositionOnBoard);
            }
        }

        private void OnMouseDown()
        {
            if (!GameLocker.IsLocked)
            {
                SwipeDetector.TilePressDetected(PositionOnBoard);
            }
        }

        private void OnMouseUp()
        {
            if (!GameLocker.IsLocked)
            {
                SwipeDetector.TileReleaseDetected();
            }
        }

        public class Factory : Factory<GameTile>
        {

        }
    }
}