﻿using Zenject;

namespace Assets.Scripts
{
    public class GameCard : GameEntity, IGameCard
    {
        [Inject]
        private readonly ICardResolver _cardResolver;

        public override void UpdatePosition(PositionOnBoard newPosition)
        {
            GetComponent<MoveComponent>().Move(newPosition);
            UpdateName();
        }

        public Card Card
        {
            get { return Entity as Card;}
        }

        public override void Clear()
        {
            GetComponent<ClearComponent>().Clear();
            _cardResolver.ReturnCard(Card);
        }

        private void UpdateName()
        {
            name = Entity.ToString();
        }

        public class Factory : Factory<GameEntity>
        {

        }
    }
}