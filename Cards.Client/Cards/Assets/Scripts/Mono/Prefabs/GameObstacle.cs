﻿using Zenject;

namespace Assets.Scripts
{
    public class GameObstacle : GameEntity
    {
        public override void Clear()
        {
            
        }

        public override void UpdatePosition(PositionOnBoard newPosition)
        {
            
        }

        public class Factory : Factory<GameEntity>
        {

        }
    }
}