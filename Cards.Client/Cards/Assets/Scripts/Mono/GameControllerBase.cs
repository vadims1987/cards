﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts
{
    public abstract class GameControllerBase : MonoBehaviour
    {
        [Inject]
        protected ILevelManager LevelManager { get; set; }

        [Inject]
        private CardSwipedSignal CardSwipedSignal { get; set; }

        [Inject]
        private IGrid Grid { get; set; }

        public void Awake()
        {
            CardSwipedSignal += SwipeCards;
        }

        public void Start()
        {
            GameLocker.Lock();
            StartCoroutine(StartGame());
        }

        protected abstract IEnumerator StartGame();

        private void SwipeCards(CardSwipedSignalArgs e)
        {
            StartCoroutine(PlaySwipe(e));
        }

        protected abstract IEnumerator PlaySwipe(CardSwipedSignalArgs e);

        protected IEnumerator PlaySwipeBase(CardSwipedSignalArgs e)
        {
            GameLocker.Lock();
            if (!Grid.IsLegalSwipe(e.FromPosition, e.ToPosition))
            {
                GameLocker.Unlock();
                yield break;
            }

            yield return StartCoroutine(LevelManager.PlayTurn(e.FromPosition, e.ToPosition));
        }
    }
}
