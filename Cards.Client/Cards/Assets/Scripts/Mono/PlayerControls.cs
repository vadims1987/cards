﻿using Assets.Scripts.Models.Game;
using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerControls : MonoBehaviour
    {
        public static PlayerControls Instance;

        public GameState GameState = new GameState();

        public void Awake()
        {
            if (Instance == null)
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
            else
            {
                Destroy(gameObject);    
            }
        }
    }
}
