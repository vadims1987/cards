﻿using System.Collections;

namespace Assets.Scripts
{
    public class TestController : GameControllerBase
    {
        protected override IEnumerator StartGame()
        {
            yield return LevelManager.StartGame();
            GameLocker.Unlock();
        }

        protected override IEnumerator PlaySwipe(CardSwipedSignalArgs e)
        {
            yield return PlaySwipeBase(e);
            GameLocker.Unlock();
        }
    }
}
