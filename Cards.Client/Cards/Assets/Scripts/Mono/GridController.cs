﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts
{
    public class GridController : MonoBehaviour
    {
        [Inject]
        private ILevelSettings LevelSettings { get; set; }

        [Inject]
        private GameTile.Factory GameTileFactory { get; set; }

        public void Start()
        {
            AddGridTiles();
        }

        private void AddGridTiles()
        {
            for (var i = 0; i < LevelSettings.NumberOfColumns; i++)
            {
                for (var j = 0; j < LevelSettings.NumberOfRows; j++)
                {
                    var tilePosition = new PositionOnBoard(j, i);
                    AddEmptyTile(tilePosition);
                }
            }
        }

        private void AddEmptyTile(PositionOnBoard tilePosition)
        {
            var emptyTile = GameTileFactory.Create();

            emptyTile.PositionOnBoard = tilePosition;
            emptyTile.name = "GridTile";
            emptyTile.transform.parent = transform;
        }
    }
}
