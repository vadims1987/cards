﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class TextComponent : MonoBehaviour
    {
        protected string Text
        {
            set { GetComponent<GUIText>().text = value; }
        }

        protected IEnumerator UpdateTextCoroutine(string text)
        {
            yield return new WaitForSeconds(AnimationConstants.ClearAnimationTime);
            Text = text;
        }
    }
}
