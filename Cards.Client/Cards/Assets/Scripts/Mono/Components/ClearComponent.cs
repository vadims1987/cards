﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class ClearComponent : MonoBehaviour
    {
        public AnimationClip ClearAnimation;

        protected GameCard GameCard;

        public void Awake()
        {
            GameCard = GetComponent<GameCard>();
        }

        public void Clear()
        {
            StartCoroutine(ClearCoroutine());
        }

        private IEnumerator ClearCoroutine()
        {
            var animator = GetComponent<Animator>();

            animator.Play(ClearAnimation.name);
            yield return new WaitForSeconds(ClearAnimation.length);
            Destroy(gameObject);
        }
    }
}
