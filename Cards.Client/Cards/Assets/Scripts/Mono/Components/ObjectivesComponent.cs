﻿using System.Collections.Generic;
using Zenject;

namespace Assets.Scripts.Mono.Components
{
    public class ObjectivesComponent : TextComponent
    {
        [Inject]
        private ObjectivesUpdatedSignal ObjectivesUpdatedSignal { get; set; }

        [Inject]
        private ILevelSettings LevelSettings { get; set; }

        public void Awake()
        {
            ObjectivesUpdatedSignal += UpdateObjectives;

            Text = CreateObjectivesText(LevelSettings.LevelObjectives);
        }

        private void UpdateObjectives(ObjectivesUpdatedSignalArgs args)
        {
            Text = CreateObjectivesText(args.LevelObjectives);
        }

        private string CreateObjectivesText(ILevelObjectives levelObjectives)
        {
            var objectivesText = "";

            objectivesText += CreateLegalHandsText(levelObjectives.TargetLegalHands);
            objectivesText += CreateScoreText(levelObjectives.TargetScore);

            return objectivesText;
        }

        private string CreateScoreText(Objective targetScore)
        {
            var currentCount = targetScore.CurrentCount >= targetScore.TargetCount
                ? targetScore.TargetCount
                : targetScore.CurrentCount;

            return targetScore.TargetCount == 0 ? ""
                : string.Format(" Points: {0}/{1}", currentCount, targetScore.TargetCount);
        }

        private string CreateLegalHandsText(IEnumerable<TargetLegalHand> targetLegalHands)
        {
            var objectivesText = "";

            foreach (var levelObjectivesTargetLegalHand in targetLegalHands)
            {
                var currentCount = levelObjectivesTargetLegalHand.CurrentCount;
                var targetCount = levelObjectivesTargetLegalHand.TargetCount;

                if (currentCount >= targetCount)
                {
                    currentCount = targetCount;
                }

                var legalHandName = LegalHandsHelper.GetLegalHandName(levelObjectivesTargetLegalHand.LegalHand);
                objectivesText += string.Format(legalHandName + " {0}/{1}", currentCount, targetCount);
            }

            return objectivesText;
        }
    }
}
