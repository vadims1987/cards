﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts
{
    public class MoveComponent : MonoBehaviour
    {
        private IEnumerator moveCoroutine;

        [Inject]
        private IWorldPositionHelper WorldPositionHelper { get; set; }

        public void Move(PositionOnBoard newPosition)
        {
            if (moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
            }

            moveCoroutine = MoveCoroutine(newPosition, AnimationConstants.MoveAnimationTime);
            StartCoroutine(moveCoroutine);
        }

        private IEnumerator MoveCoroutine(PositionOnBoard newPosition, float time)
        {
            var startPosition = transform.position;
            var endPosition = GetNewWorldPosiotion(newPosition);

            for (var t = 0f; t < time; t += Time.deltaTime)
            {
                transform.position = Vector3.Lerp(startPosition, endPosition, t / time);
                yield return 0;
            }

            transform.position = endPosition;
        }

        private Vector3 GetNewWorldPosiotion(PositionOnBoard newPosition)
        {
            var gridPostion = GridPositionHelper.GetGridPosition(newPosition);
            return WorldPositionHelper.GetEntityWorldPosition(gridPostion, transform.parent.position);
        }
    }
}
