﻿using Zenject;

namespace Assets.Scripts
{
    public class TurnsComponent : TextComponent
    {
        [Inject]
        private TurnUpdatedSignal TurnUpdatedSignal { get; set; }

        [Inject]
        private ILevelSettings LevelSettings { get; set; }

        private int _turnsLeft;

        public void Awake()
        {
            TurnUpdatedSignal += UpdateScore;
            Text = LevelSettings.MaxNumberOfTurns.ToString();
        }

        private void UpdateScore(TurnUpdatedSignalArgs args)
        {
            _turnsLeft = args.TurnsLeft;
            Text = _turnsLeft.ToString();
        }
    }
}
