﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts
{
    public class LevelLoaderComponent : MonoBehaviour
    {
        [Inject]
        private ILevelLoader LevelLoader { get; set; }

        public int LevelNumber;

        public void Start()
        {
            var  button = GetComponent<Button>();

            if (PlayerControls.Instance.GameState.CurrentLevel < LevelNumber)
            {
                button.interactable = false;
            }

            button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            var level = new Level {Number = LevelNumber};
            LevelLoader.LoadLevel(level);
        }
    }
}
