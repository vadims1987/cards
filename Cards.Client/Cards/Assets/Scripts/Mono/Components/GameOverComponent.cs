﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts
{
    public class GameOverComponent : TextComponent
    {
        [Inject]
        private GameLostSignal GameLostSignal { get; set; }

        [Inject]
        private GameWonSignal GameWonSignal { get; set; }

        public void Awake()
        {
            GameLostSignal += OnGameLost;
            GameWonSignal += OnGameWon;

            gameObject.SetActive(false);
        }

        private void OnGameLost(GameLostSignalArgs args)
        {
            GetComponent<GUIText>().text = "Game Lost";
            gameObject.SetActive(true);
        }

        private void OnGameWon(GameWonSignalArgs args)
        {
            GetComponent<GUIText>().text = "Game Won";
            gameObject.SetActive(true);
        }
    }
}
