﻿using Zenject;

namespace Assets.Scripts
{
    public class ScoreComponent : TextComponent
    {
        [Inject]
        private ObjectivesUpdatedSignal ObjectivesUpdatedSignal { get; set; }

        public void Awake()
        {
            ObjectivesUpdatedSignal += UpdateScore;

            Text = "0";
        }

        private void UpdateScore(ObjectivesUpdatedSignalArgs args)
        {
            var currentScore = args.LevelObjectives.TargetScore.CurrentCount;
            StartCoroutine(UpdateTextCoroutine(currentScore.ToString()));
        }
    }
}
