﻿using System.Collections;
using Zenject;

namespace Assets.Scripts
{
    public class GameController : GameControllerBase
    {
        [Inject]
        private GameWonSignal GameWonSignal { get; set; }

        [Inject]
        private GameLostSignal GameLostSignal { get; set; }

        [Inject]
        private ILevelLoader LevelLoader { get; set; }

        protected override IEnumerator StartGame()
        {
            yield return LevelManager.StartGame();
            EndTurn();
        }

        protected override IEnumerator PlaySwipe(CardSwipedSignalArgs e)
        {
            yield return PlaySwipeBase(e);
            EndTurn();
        }

        private void EndTurn()
        {
            switch (LevelManager.LevelStatus)
            {
                case LevelStatus.Won:
                    WinLevel();
                    break;

                case LevelStatus.Lost:
                    LoseLevel();
                    break;

                case LevelStatus.InProgress:
                    GameLocker.Unlock();
                    break;
            }
        }

        private void WinLevel()
        {
            GameWonSignal.Fire(new GameWonSignalArgs());
            PlayerControls.Instance.GameState.CurrentLevel++;
            FinishLevel();
        }

        private void LoseLevel()
        {
            GameLostSignal.Fire(new GameLostSignalArgs());
            FinishLevel();
        }

        private void FinishLevel()
        {
            LevelLoader.LoadMainScene();
        }
    }
}
