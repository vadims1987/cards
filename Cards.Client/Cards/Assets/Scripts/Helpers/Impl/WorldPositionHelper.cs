﻿using UnityEngine;

namespace Assets.Scripts
{
    public class WorldPositionHelper : IWorldPositionHelper
    {
        private readonly ILevelSettings _levelSettings;

        public WorldPositionHelper(ILevelSettings levelSettings)
        {
            _levelSettings = levelSettings;
        }

        public Vector3 GetEntityWorldPosition(Vector3 entityPosition)
        {
            return GetEntityWorldPositionInternal(entityPosition, new Vector3(0, 0, 0));
        }

        public Vector3 GetEntityWorldPosition(Vector3 entityPosition, Vector3 gridWorldPosition)
        {
            return GetEntityWorldPositionInternal(entityPosition, gridWorldPosition);
        }

        private Vector3 GetEntityWorldPositionInternal(Vector3 entityPosition, Vector3 gridWorldPosition)
        {
            var xPosition = GetXPosition(entityPosition, gridWorldPosition);
            var yPosition = GetYPosition(entityPosition, gridWorldPosition);

            return new Vector3(xPosition, yPosition, 0);
        }

        private float GetXPosition(Vector3 entityPosition, Vector3 gridWorldPosition)
        {
            var centeringOffset = _levelSettings.NumberOfColumns / 2.7f;
            return gridWorldPosition.x + entityPosition.x / 1.1f - centeringOffset;
        }

        private float GetYPosition(Vector3 entityPosition, Vector3 gridWorldPosition)
        {
            var centeringOffset = _levelSettings.NumberOfRows / 2.45f;
            return gridWorldPosition.y - entityPosition.y / 1.05f + centeringOffset;
        }
    }
}
