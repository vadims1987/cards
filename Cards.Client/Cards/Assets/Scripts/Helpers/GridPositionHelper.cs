﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GridPositionHelper
    {
        public static Vector3 GetGridPosition(PositionOnBoard positionOnBoard)
        {
            return new Vector3(positionOnBoard.Column, positionOnBoard.Row, 0);
        }
    }
}
