﻿using UnityEngine;

namespace Assets.Scripts
{
    public interface IWorldPositionHelper
    {
        Vector3 GetEntityWorldPosition(Vector3 entityPosition);
        Vector3 GetEntityWorldPosition(Vector3 entityPosition, Vector3 gridWorldPosition);
    }
}
