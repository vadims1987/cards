﻿
namespace Assets.Scripts
{
    public enum Suit
    {
        None = 0,
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }
}
