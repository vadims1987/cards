﻿namespace Assets.Scripts
{
    public class EntityUpdatedParam
    {
        public IEntity NewCreatedEntity { get; set; }
        public PositionOnBoard FromPosition { get; set; }
        public PositionOnBoard ToPosition { get; set; }
    }
}
