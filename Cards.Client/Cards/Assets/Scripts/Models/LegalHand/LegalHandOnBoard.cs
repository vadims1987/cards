﻿using System.Collections;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class LegalHandOnBoard : IEnumerable<CardOnBoard>
    {
        public IList<CardOnBoard> CardsOnBoard { get; set; }

        public IEnumerator<CardOnBoard> GetEnumerator()
        {
            return CardsOnBoard.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
