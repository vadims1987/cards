﻿namespace Assets.Scripts
{
    public interface ILegalHandExtractor
    {
        FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand);
    }
}
