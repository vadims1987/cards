﻿namespace Assets.Scripts
{
    public interface ICandidateLegalHandsExtractor
    {
        CurrentCandidateLegalHands GetCurrentCandidateLegalHands(IGrid grid);
    }
}
