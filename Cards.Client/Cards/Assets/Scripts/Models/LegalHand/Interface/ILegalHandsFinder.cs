﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public interface ILegalHandsFinder
    {
        IList<FoundLegalHand> FindLegalHands(IGrid grid);
    }
}
