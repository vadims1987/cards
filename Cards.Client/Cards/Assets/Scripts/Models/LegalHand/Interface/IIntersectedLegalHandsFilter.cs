﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public interface IIntersectedLegalHandsFilter
    {
        IList<FoundLegalHand> FilterIntersectedLegalHands(IList<FoundLegalHand> foundLegalHands);
    }
}
