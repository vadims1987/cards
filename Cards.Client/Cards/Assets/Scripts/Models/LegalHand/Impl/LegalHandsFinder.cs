﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public class LegalHandsFinder : ILegalHandsFinder
    {
        private readonly IDictionary<LegalHand, ILegalHandExtractor> _legalHandsRules = new Dictionary<LegalHand, ILegalHandExtractor>();

        private readonly ICandidateLegalHandsExtractor _candidateLegalHandsExtractor;

        private readonly IIntersectedLegalHandsFilter _intersectedLegalHandsFilter;

        public LegalHandsFinder(ICandidateLegalHandsExtractor candidateLegalHandsExtractor,
            IIntersectedLegalHandsFilter intersectedLegalHandsFilter)
        {
            _candidateLegalHandsExtractor = candidateLegalHandsExtractor;
            _intersectedLegalHandsFilter = intersectedLegalHandsFilter;

            //_legalHandsRules.Add(LegalHand.StraightFlush, new StraightFlushExtractor());
            _legalHandsRules.Add(LegalHand.FourOfKind, new FourOfKindExtractor());
            //_legalHandsRules.Add(LegalHand.FullHouse, new FullHouseExtractor());
            //_legalHandsRules.Add(LegalHand.Flush, new FlushExtractor());
            // _legalHandsRules.Add(LegalHand.Straight, new StraightExtractor());
            _legalHandsRules.Add(LegalHand.ThreeOfKind, new ThreeOfKindExtractor());
            _legalHandsRules.Add(LegalHand.TwoPairs, new TwoPairsExtractor());
            _legalHandsRules.Add(LegalHand.Pair, new PairExtractor());
            _legalHandsRules.Add(LegalHand.None, new NoneExtractor());
        }

        public IList<FoundLegalHand> FindLegalHands(IGrid grid)
        {
            var foundLegalHands = GetFoundLegalHands(grid);

            return foundLegalHands;
        }

        private IList<FoundLegalHand> GetFoundLegalHands(IGrid grid)
        {
            var foundLegalHands = new List<FoundLegalHand>();
            var currentCandidateLegalHands = _candidateLegalHandsExtractor.GetCurrentCandidateLegalHands(grid);

            foreach (var candidateLegalHand in currentCandidateLegalHands)
            {
                foreach (var current in _legalHandsRules.Keys)
                {
                    var foundLegalHand = _legalHandsRules[current].GetLegalHand(candidateLegalHand);

                    if (foundLegalHand.LegalHand == LegalHand.None) continue;

                    foundLegalHands.Add(foundLegalHand);
                    break;
                }
            }

            var filteredFoundLegalHands = _intersectedLegalHandsFilter.FilterIntersectedLegalHands(foundLegalHands);

            return filteredFoundLegalHands;
        }
    }
}