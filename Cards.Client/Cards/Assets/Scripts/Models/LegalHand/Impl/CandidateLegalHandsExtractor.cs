﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts
{
    public class CandidateLegalHandsExtractor : ICandidateLegalHandsExtractor
    {
        private readonly ILevelSettings _levelSettings;

        public CandidateLegalHandsExtractor(ILevelSettings levelSettings)
        {
            _levelSettings = levelSettings;
        }

        public CurrentCandidateLegalHands GetCurrentCandidateLegalHands(IGrid grid)
        {
            var horizontalCandidateHands = GetHorizontalCandidateLegalHands(grid);
            var verticalCandidateHands = GetVerticalCandidateLegalHands(grid);

            var candidateLegalHands = horizontalCandidateHands.Concat(verticalCandidateHands).ToList();

            return RemoveObstacles(candidateLegalHands);

        }

        private CurrentCandidateLegalHands RemoveObstacles(IList<CandidateLegalHand> lst)
        {
            foreach (var candidateLegalHand in lst)
            {
                candidateLegalHand.CardOnBoardPairs = FilterObstacles(candidateLegalHand.CardOnBoardPairs);
                candidateLegalHand.CardOnBoardQuartets = FilterObstacles(candidateLegalHand.CardOnBoardQuartets);
                candidateLegalHand.CardOnBoardTriplets = FilterObstacles(candidateLegalHand.CardOnBoardTriplets);
            }

            return new CurrentCandidateLegalHands() { CandidateLegalHands = lst };
        }

        private List<LegalHandOnBoard> FilterObstacles(IEnumerable<LegalHandOnBoard> cards)
        {
            return cards.Where
                (hand => !hand.CardsOnBoard.Any
                    (
                        x => x.Card.Suit == Suit.None || x.Card.Rank == Rank.None
                    )
                ).ToList();
        }

        private IEnumerable<CandidateLegalHand> GetHorizontalCandidateLegalHands(IGrid grid)
        {
            var candidateLegalHands = new List<CandidateLegalHand>();

            for (var x = 0; x < _levelSettings.NumberOfRows; x++)
            {
                var cardsOnboards = GetHorizontalCardsOnBoard(grid, x);
                var candidateLegalHand = ExtractCandidateLegalHand(cardsOnboards);

                candidateLegalHands.Add(candidateLegalHand);
            }

            return candidateLegalHands;
        }

        private List<CardOnBoard> GetHorizontalCardsOnBoard(IGrid grid, int xPosition)
        {
            var cardsOnBoard = new List<CardOnBoard>();

            for (var y = 0; y < _levelSettings.NumberOfColumns; y++)
            {
                var position = new PositionOnBoard(xPosition, y);
                var cardOnbBoard = GetCardOnBoard(grid, position);

                cardsOnBoard.Add(cardOnbBoard);
            }

            return cardsOnBoard;
        }

        private IEnumerable<CandidateLegalHand> GetVerticalCandidateLegalHands(IGrid board)
        {
            var candidateLegalHands = new List<CandidateLegalHand>();

            for (var y = 0; y < _levelSettings.NumberOfColumns; y++)
            {
                var cardsOnboards = GetVerticalCardsOnBoard(board, y);
                var candidateLegalHand = ExtractCandidateLegalHand(cardsOnboards);

                candidateLegalHands.Add(candidateLegalHand);
            }

            return candidateLegalHands;
        }

        private List<CardOnBoard> GetVerticalCardsOnBoard(IGrid grid, int yPosition)
        {
            var cardsOnBoard = new List<CardOnBoard>();

            for (var x = 0; x < _levelSettings.NumberOfRows; x++)
            {
                var position = new PositionOnBoard(x, yPosition);
                var cardOnbBoard = GetCardOnBoard(grid, position);

                cardsOnBoard.Add(cardOnbBoard);
            }

            return cardsOnBoard;
        }

        private CardOnBoard GetCardOnBoard(IGrid grid, PositionOnBoard position)
        {
            var gameCard = grid[position] as IGameCard;
            var card = gameCard == null ? new Card(Suit.None, Rank.None) : gameCard.Card;

            return new CardOnBoard
            {
                Card = card,
                PositionOnBoard = position
            };
        }

        private CandidateLegalHand ExtractCandidateLegalHand(List<CardOnBoard> cardsOnBoard)
        {
            var candidateLegalHand = new CandidateLegalHand
            {
                CardOnBoardPairs = GetCards(cardsOnBoard, 2),
                CardOnBoardTriplets = GetCards(cardsOnBoard, 3),
                CardOnBoardQuartets = GetCards(cardsOnBoard, 4)
            };


            return candidateLegalHand;
        }

        private IList<LegalHandOnBoard> GetCards(List<CardOnBoard> cardsOnBoard, int cardsCount)
        {
            var handsOnboard = new List<LegalHandOnBoard>();

            for (var i = 0; i + cardsCount <= cardsOnBoard.Count; i++)
            {
                handsOnboard.Add(new LegalHandOnBoard
                {
                    CardsOnBoard = cardsOnBoard.GetRange(i, cardsCount)
                });
            }

            return handsOnboard;
        }
    }
}
