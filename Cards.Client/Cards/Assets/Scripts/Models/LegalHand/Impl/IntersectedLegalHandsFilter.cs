﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts
{
    public class IntersectedLegalHandsFilter : IIntersectedLegalHandsFilter
    {
        public IList<FoundLegalHand> FilterIntersectedLegalHands(IList<FoundLegalHand> foundLegalHands)
        {
            var lstResult = new List<FoundLegalHand>();

            foreach (var firstLoopItem in foundLegalHands)
            {
                var isAllLegalIntersections = true;

                foreach (var secondLoopItem in foundLegalHands)
                {
                    if (firstLoopItem != secondLoopItem && !CheckForLegalIntersection(firstLoopItem, secondLoopItem))
                    {
                        isAllLegalIntersections = false;
                    }
                }

                if (isAllLegalIntersections)
                {
                    lstResult.Add(firstLoopItem);
                }
            }

            return lstResult;
        }

        private bool CheckForLegalIntersection(FoundLegalHand firstItem, FoundLegalHand secondItem)
        {
            var intersectedResult = firstItem.LegalHandOnBoard.CardsOnBoard
                .Intersect(secondItem.LegalHandOnBoard.CardsOnBoard).ToList();

            if (intersectedResult.Count > 1 && firstItem.LegalHand < secondItem.LegalHand)
            {
                return false;
            }

            if (intersectedResult.Count != 1) return true;

            if (IsSameAxis(firstItem, secondItem, intersectedResult) && firstItem.LegalHand < secondItem.LegalHand)
            {
                return false;
            }
            return true;
        }

        private bool IsSameAxis(FoundLegalHand firstItem, FoundLegalHand secondItem, List<CardOnBoard> intersectedResult)
        {
            var localFirstItem = firstItem.LegalHandOnBoard.CardsOnBoard.Except(intersectedResult).ToList();
            var localsecondItem = secondItem.LegalHandOnBoard.CardsOnBoard.Except(intersectedResult).ToList();

            return localFirstItem.Any(firstHandCard =>
                         localsecondItem.Any(secondHandCard =>
                                   secondHandCard.PositionOnBoard.Column == firstHandCard.PositionOnBoard.Column
                                       || secondHandCard.PositionOnBoard.Row == firstHandCard.PositionOnBoard.Row));
        }
    }
}
