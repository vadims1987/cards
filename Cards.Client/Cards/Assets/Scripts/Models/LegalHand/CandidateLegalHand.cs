﻿using System.Collections;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class CandidateLegalHand
    {
        public IList<LegalHandOnBoard> CardOnBoardPairs { get; set; }
        public IList<LegalHandOnBoard> CardOnBoardTriplets { get; set; }
        public IList<LegalHandOnBoard> CardOnBoardQuartets { get; set; }
        public LegalHandOnBoard CardOnBoardQuintet { get; set; }
    }

    public class CurrentCandidateLegalHands : IEnumerable<CandidateLegalHand>
    {
        public IList<CandidateLegalHand> CandidateLegalHands { get; set; }

        public IEnumerator<CandidateLegalHand> GetEnumerator()
        {
            return CandidateLegalHands.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
