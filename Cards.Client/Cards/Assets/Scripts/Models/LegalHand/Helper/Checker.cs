﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts
{
    public static class Checker
    {
        public static bool IsSameRank(this IList<CardOnBoard> cardsOnBoard)
        {
            if (IsEmptyOrNull(cardsOnBoard))
                return false;
            return cardsOnBoard.All(x => x.Card.Rank == cardsOnBoard[0].Card.Rank);
        }
        public static bool IsSameSuit(this IList<CardOnBoard> cardsOnBoard)
        {
            if (IsEmptyOrNull(cardsOnBoard))
                return false;
            return cardsOnBoard.All(x => x.Card.Suit == cardsOnBoard[0].Card.Suit);
        }
        public static bool IsInSequenceRank(this IList<CardOnBoard> cardsOnBoard)
        {
            if (IsEmptyOrNull(cardsOnBoard))
                return false;

            var orderedList = cardsOnBoard.OrderBy(cardOnBoard => (cardOnBoard.Card.Rank)).ToList();

            for (int i = 1; i < orderedList.Count; i++)
            {
                if ((orderedList[0].Card.Rank) + i != (orderedList[i].Card.Rank))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool IsEmptyOrNull<T>(IList<T> lst)
        {
            if (lst == null || lst.Count <= 0)
                return true;
            return false;
        }
    }
}
