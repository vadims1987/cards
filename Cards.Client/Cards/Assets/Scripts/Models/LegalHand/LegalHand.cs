﻿
namespace Assets.Scripts
{
    public enum LegalHand
    {
        None = 0,
        Pair,
        TwoPairs,
        ThreeOfKind,
        Straight,
        Flush,
        FullHouse,
        FourOfKind,
        StraightFlush,
        Royal
    }
}
