﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public static class LegalHandsHelper
    {
        private static readonly IDictionary<LegalHand, string> LegalHandsNamesMap = new Dictionary<LegalHand, string>();

        static LegalHandsHelper()
        {
            LegalHandsNamesMap.Add(LegalHand.Pair, "Pair");
        }

        public static string GetLegalHandName(LegalHand legalHand)
        {
            return LegalHandsNamesMap[legalHand];
        }
    }
}
