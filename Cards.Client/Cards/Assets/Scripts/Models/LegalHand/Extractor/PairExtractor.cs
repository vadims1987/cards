﻿namespace Assets.Scripts
{
    public class PairExtractor : ILegalHandExtractor
    {
        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            var result = new FoundLegalHand { LegalHand = LegalHand.None, LegalHandOnBoard = null };

            foreach (var pair in candidateLegalHand.CardOnBoardPairs)
            {
                if (HavePair(pair))
                {
                    result.LegalHand = LegalHand.Pair;
                    result.LegalHandOnBoard = pair;
                    break;
                }
            }
            return result;
        }

        private bool HavePair(LegalHandOnBoard pair)
        {
            return pair.CardsOnBoard.IsSameRank();
        }
    }
}
