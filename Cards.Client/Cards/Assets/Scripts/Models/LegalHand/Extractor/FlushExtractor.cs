﻿namespace Assets.Scripts
{
    public class FlushExtractor : ILegalHandExtractor
    {

        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            var legalHand = new FoundLegalHand { LegalHand = LegalHand.None, LegalHandOnBoard = null };

            if (HaveFlush(candidateLegalHand.CardOnBoardQuintet))
            {
                legalHand.LegalHand = LegalHand.Flush;
                legalHand.LegalHandOnBoard = candidateLegalHand.CardOnBoardQuintet;
            }
            return legalHand;
        }

        private bool HaveFlush(LegalHandOnBoard hand)
        {
            return hand.CardsOnBoard.IsSameSuit();
        }
    }
}

