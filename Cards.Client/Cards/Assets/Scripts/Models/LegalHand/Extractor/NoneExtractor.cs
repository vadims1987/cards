﻿
namespace Assets.Scripts
{
    public class NoneExtractor : ILegalHandExtractor
    {
        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            return new FoundLegalHand
            {
                LegalHand = LegalHand.None,
                LegalHandOnBoard = null
            };
        }
    }
}
