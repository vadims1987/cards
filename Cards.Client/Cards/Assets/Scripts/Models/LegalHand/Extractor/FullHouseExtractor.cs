﻿namespace Assets.Scripts
{
    public class FullHouseExtractor : ILegalHandExtractor
    {
        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            var result = new FoundLegalHand { LegalHand = LegalHand.None, LegalHandOnBoard = null };

            foreach (var triple in candidateLegalHand.CardOnBoardTriplets)
            {
                if (IsSameRank(triple))
                {
                    foreach (var pair in candidateLegalHand.CardOnBoardPairs)
                    {
                        if (IsSameRank(pair))
                        {
                            result.LegalHand = LegalHand.FullHouse;
                            result.LegalHandOnBoard = candidateLegalHand.CardOnBoardQuintet;
                            return result;
                        }
                    }
                    return result;
                }
            }
            return result;
        }

        private bool IsSameRank(LegalHandOnBoard hand)
        {
            return hand.CardsOnBoard.IsSameRank();
        }
    }
}
