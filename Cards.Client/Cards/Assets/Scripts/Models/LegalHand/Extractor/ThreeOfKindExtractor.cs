﻿namespace Assets.Scripts
{
    public class ThreeOfKindExtractor : ILegalHandExtractor
    {
        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            var result = new FoundLegalHand { LegalHand = LegalHand.None, LegalHandOnBoard = null };

            foreach (var triple in candidateLegalHand.CardOnBoardTriplets)
            {
                if (HaveTriple(triple))
                {
                    result.LegalHandOnBoard = triple;
                    result.LegalHand = LegalHand.ThreeOfKind;
                    break;
                }
            }
            return result;
        }

        private bool HaveTriple(LegalHandOnBoard triple)
        {
            return triple.CardsOnBoard.IsSameRank();
        }
    }
}
