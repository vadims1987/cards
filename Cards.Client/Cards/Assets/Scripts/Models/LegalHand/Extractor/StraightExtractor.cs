﻿
namespace Assets.Scripts
{
    public class StraightExtractor : ILegalHandExtractor
    {
        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            var legalHand = new FoundLegalHand { LegalHand = LegalHand.None, LegalHandOnBoard = null };

            if (HaveStraight(candidateLegalHand.CardOnBoardQuintet))
            {
                legalHand.LegalHand = LegalHand.Straight;
                legalHand.LegalHandOnBoard = candidateLegalHand.CardOnBoardQuintet;
            }
            return legalHand;
        }

        private bool HaveStraight(LegalHandOnBoard hand)
        {
            return hand.CardsOnBoard.IsInSequenceRank();
        }
    }
}
