﻿namespace Assets.Scripts
{
    public class TwoPairsExtractor : ILegalHandExtractor
    {
        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            int counter = 0;
            int numberOfPairsForLegalHands = 2;
            var result = new FoundLegalHand { LegalHand = LegalHand.None, LegalHandOnBoard = null };

            foreach (var pair in candidateLegalHand.CardOnBoardPairs)
            {
                if (IsSameRank(pair))
                {
                    counter++;
                }
            }

            if (counter == numberOfPairsForLegalHands)
            {
                result.LegalHand = LegalHand.TwoPairs;
                result.LegalHandOnBoard = candidateLegalHand.CardOnBoardQuintet;
                return result;
            }
            return result;
        }

        private bool IsSameRank(LegalHandOnBoard hand)
        {
            return hand.CardsOnBoard.IsSameRank();
        }
    }
}
