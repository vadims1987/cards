﻿namespace Assets.Scripts
{
    public class FourOfKindExtractor : ILegalHandExtractor
    {
        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            var result = new FoundLegalHand { LegalHand = LegalHand.None, LegalHandOnBoard = null };

            //shouldn't check for best quarter because two card is always common
            foreach (var quarter in candidateLegalHand.CardOnBoardQuartets)
            {
                if (HaveQuarter(quarter))
                {
                    result.LegalHand = LegalHand.FourOfKind;
                    result.LegalHandOnBoard = quarter;
                    break;
                }
            }
            return result;
        }

        private bool HaveQuarter(LegalHandOnBoard quarter)
        {
            return quarter.CardsOnBoard.IsSameRank();
        }
    }
}
