﻿
namespace Assets.Scripts
{
    public class StraightFlushExtractor : ILegalHandExtractor
    {
        public FoundLegalHand GetLegalHand(CandidateLegalHand candidateLegalHand)
        {
            var legalHand = new FoundLegalHand { LegalHand = LegalHand.None, LegalHandOnBoard = null };

            if (HaveStraightFlush(candidateLegalHand.CardOnBoardQuintet))
            {
                legalHand.LegalHand = LegalHand.StraightFlush;
                legalHand.LegalHandOnBoard = candidateLegalHand.CardOnBoardQuintet;
            }
            return legalHand;
        }

        private bool HaveStraightFlush(LegalHandOnBoard hand)
        {
            return hand.CardsOnBoard.IsInSequenceRank() && hand.CardsOnBoard.IsSameSuit();
        }
    }
}
