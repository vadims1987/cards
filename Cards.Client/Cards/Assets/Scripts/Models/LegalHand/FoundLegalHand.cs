﻿namespace Assets.Scripts
{
    public class FoundLegalHand
    {
        public LegalHand LegalHand { get; set; }
        public LegalHandOnBoard LegalHandOnBoard { get; set; }
    }
}
