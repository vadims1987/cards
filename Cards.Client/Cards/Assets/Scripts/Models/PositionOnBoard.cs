﻿using System;

namespace Assets.Scripts
{
    public class PositionOnBoard : ICloneable
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public PositionOnBoard(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public override string ToString()
        {
            return "(" + Row + ", " + Column + ")";
        }

        public override bool Equals(object obj)
        {
            var cardPositionOnBoard = obj as PositionOnBoard;

            return cardPositionOnBoard.Row == Row && cardPositionOnBoard.Column == Column;
        }

        public override int GetHashCode()
        {
            return Row.GetHashCode() + Column.GetHashCode();
        }

        public object Clone()
        {
            return new PositionOnBoard(Row, Column);
        }
    }
}
