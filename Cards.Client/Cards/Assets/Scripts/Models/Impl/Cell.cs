﻿
namespace Assets.Scripts
{
    public class Cell : ICell
    {
        public PositionOnBoard PositionOnBoard
        {
            get { return _positionOnBoard; } 
        }

        public IGameEntity Entity { get; set; }

        private readonly PositionOnBoard _positionOnBoard;

        public Cell(PositionOnBoard positionOnBoard)
        {
            _positionOnBoard = positionOnBoard;
        }
   }
}
