﻿
namespace Assets.Scripts
{
    public class ObstacleOnBoard : IEntityOnBoard
    {
        public Obstacle Obstacle { get; set; }

        public IEntity Entity
        {
            get { return Obstacle; }
        }

        public PositionOnBoard PositionOnBoard { get; set; }
    }
}
