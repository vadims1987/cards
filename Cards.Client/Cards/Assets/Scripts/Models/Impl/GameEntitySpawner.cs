﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts
{
    public class GameEntitySpawner : IGameEntitySpawner
    {
        private readonly SpritesMap _spritesMap = new SpritesMap();

        private readonly GameCard.Factory _gameCardFactory;

        private readonly GameObstacle.Factory _gameObstacleFactory;

        private readonly IWorldPositionHelper _worldPositionHelper;

        private GameEntitySpawner(GameCard.Factory gameCardFactory, 
            GameObstacle.Factory gameObstacleFactory, 
            IWorldPositionHelper worldPositionHelper){

            _gameCardFactory = gameCardFactory;
            _gameObstacleFactory = gameObstacleFactory;
            _worldPositionHelper = worldPositionHelper;
        }

        public IGameEntity CreateGameEntity(IEntity entity, PositionOnBoard position)
        {
            if (entity is Card)
            {
                return CreateGameEntity(entity, position, _gameCardFactory);
            }

            if (entity is Obstacle)
            {
                return CreateGameEntity(entity, position, _gameObstacleFactory);
            }

            return null;
        }

        private IGameEntity CreateGameEntity(IEntity entity, PositionOnBoard position, IFactory<GameEntity> factory)
        {
            var gameEntity = factory.Create();

            var sprite = _spritesMap[entity];
            gameEntity.Init(entity, sprite);

            gameEntity.GetComponent<Transform>().position = GetEntityPosition(position);

            return gameEntity;
        }

        private Vector3 GetEntityPosition(PositionOnBoard positionOnBoard)
        {
            var position = positionOnBoard.Row == 0
                ? new PositionOnBoard(-1, positionOnBoard.Column)
                : positionOnBoard;

            var gridPostion = GridPositionHelper.GetGridPosition(position);
            return _worldPositionHelper.GetEntityWorldPosition(gridPostion);
        }
    }
}
