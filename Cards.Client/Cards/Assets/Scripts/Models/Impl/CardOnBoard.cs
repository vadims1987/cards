﻿
namespace Assets.Scripts
{
    public class CardOnBoard : IEntityOnBoard
    {
        public Card Card { get; set; }

        public IEntity Entity
        {
            get { return Card; }
        }

        public PositionOnBoard PositionOnBoard { get; set; }
    }
}
