﻿using System;
using Zenject;

namespace Assets.Scripts
{
    public class SwipeDetector : ISwipeDetector
    {
        private readonly CardSwipedSignal _cardSwipedSignal;

        private PositionOnBoard _enteredPosition;

        private PositionOnBoard _pressedPosition;

        [Inject]
        public SwipeDetector(CardSwipedSignal cardSwipedSignal)
        {
            _cardSwipedSignal = cardSwipedSignal;
        }

        public void TileEnterDetected(PositionOnBoard positionOnBoard)
        {
            _enteredPosition = positionOnBoard;
        }

        public void TilePressDetected(PositionOnBoard positionOnBoard)
        {
            _pressedPosition = positionOnBoard;
        }

        public void TileReleaseDetected()
        {
            if (!AreAdjacent())
            {
                _enteredPosition = null;
                _pressedPosition = null;
                return;
            }

            _cardSwipedSignal.Fire(new CardSwipedSignalArgs
            {
                FromPosition = _pressedPosition,
                ToPosition = _enteredPosition
            });
        }

        private bool AreAdjacent()
        {
            if (_pressedPosition == null || _enteredPosition == null)
            {
                return false;
            }

            return (_enteredPosition.Row == _pressedPosition.Row
                    && (Math.Abs(_enteredPosition.Column - _pressedPosition.Column) == 1))
                   ||
                   (_enteredPosition.Column == _pressedPosition.Column
                    && (Math.Abs(_enteredPosition.Row - _pressedPosition.Row) == 1));
        }
    }

}
