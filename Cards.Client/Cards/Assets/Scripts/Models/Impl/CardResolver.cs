﻿namespace Assets.Scripts
{
    public class CardResolver : ICardResolver
    {
        private readonly Deck _deck = new Deck();

        public Card GetCard()
        {
            return _deck.Draw();
        }

        public void ReturnCard(Card card)
        {
            _deck.Return(card);
        }
    }
}
