﻿using Zenject;

namespace Assets.Scripts
{
    public class Grid : IGrid
    {

        private readonly IGameEntitySpawner _gameEntitySpawner;

        private readonly ILevelSettings _levelSettings;

        private ICell[,] _cells;

        [Inject]
        private Grid(ILevelSettings levelSettings,
            IGameEntitySpawner gameEntitySpawner)
        {
            _levelSettings = levelSettings;
            _gameEntitySpawner = gameEntitySpawner;
        }

        public IGameEntity this[PositionOnBoard positionOnBoard]
        {
            get { return _cells[positionOnBoard.Row, positionOnBoard.Column].Entity; }
            set { _cells[positionOnBoard.Row, positionOnBoard.Column].Entity = value; }
        }

        public void Init()
        {
            InitCells();
            AddObstacles();
        }

        private void InitCells()
        {
            _cells = new ICell[_levelSettings.NumberOfRows, _levelSettings.NumberOfColumns];

            for (var i = 0; i < _levelSettings.NumberOfRows; i++)
            {
                for (var j = 0; j < _levelSettings.NumberOfColumns; j++)
                {
                    var positionOnBoard = new PositionOnBoard(i, j);
                    _cells[i, j] = new Cell(positionOnBoard);
                }
            }
        }

        private void AddObstacles()
        {
            if (_levelSettings.Obstacles == null)
            {
                return;
            }

            foreach (var obstacleOnBoard in _levelSettings.Obstacles)
            {
                AddEntity(obstacleOnBoard.Entity, obstacleOnBoard.PositionOnBoard);
            }
        }

        private void AddEntity(IEntity entity, PositionOnBoard position)
        {
            var gameEntity = _gameEntitySpawner.CreateGameEntity(entity, position);
            this[position] = gameEntity;

            if (gameEntity.CanMove)
            {
                this[position].UpdatePosition(new PositionOnBoard(0, position.Column));
            }
        }

        public bool IsLegalSwipe(PositionOnBoard fromPosition, PositionOnBoard toPosition)
        {
            return this[fromPosition].CanMove && this[toPosition].CanMove;
        }

        public void SwipeCards(PositionOnBoard fromPosition, PositionOnBoard toPosition)
        {
            SwitchCards(fromPosition, toPosition);
        }

        public void RemoveEntity(PositionOnBoard positionOnBoard)
        {
            this[positionOnBoard].Clear();
            this[positionOnBoard] = null;
        }

        public void UpdateEntity(EntityUpdatedParam e)
        {
            var newEntity = e.NewCreatedEntity != null;

            if (newEntity)
            {
                AddEntity(e.NewCreatedEntity, e.FromPosition);
            }

            var updatedCard = this[e.FromPosition];
            updatedCard.UpdatePosition(e.ToPosition);
            MoveEntity(updatedCard, e.FromPosition, e.ToPosition);
        }

        private void MoveEntity(IGameEntity entity, PositionOnBoard fromPosition, PositionOnBoard toPosition)
        {
            this[fromPosition] = null;
            this[toPosition] = entity;
        }

        private void SwitchCards(PositionOnBoard fromPosition, PositionOnBoard toPosition)
        {
            var movedCardPosition = fromPosition.Clone() as PositionOnBoard;
            var swipedOnCardPosition = toPosition.Clone() as PositionOnBoard;

            var movedCard = this[fromPosition];
            var swipedOnCard = this[toPosition];

            this[movedCardPosition] = swipedOnCard;
            this[swipedOnCardPosition] = movedCard;

            movedCard.UpdatePosition(swipedOnCardPosition);
            swipedOnCard.UpdatePosition(movedCardPosition);
        }
    }
}
