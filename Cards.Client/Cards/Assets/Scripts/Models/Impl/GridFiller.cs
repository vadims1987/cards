﻿
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class GridFiller : IGridFiller
    {
        private IGrid _grid;

        private readonly ILevelSettings _levelSettings;

        private readonly ICardResolver _cardResolver;

        private bool _fillInverse;

        private bool _continueUpdate;

        public GridFiller(ILevelSettings levelSettings, ICardResolver cardResolver)
        {
            _levelSettings = levelSettings;
            _cardResolver = cardResolver;
        }

        public bool FillGrid(IGrid grid)
        {
            _fillInverse = !_fillInverse;

            _grid = grid;
            _continueUpdate = false;

            UpdateBoard();

            return _continueUpdate;
        }

        private void UpdateBoard()
        {
            foreach (var positionOnBoard in GridPositions())
            {
                if (_grid[positionOnBoard] != null)
                {
                    UpdateEntity(positionOnBoard);
                }
            }

            AddNewEntities();
        }

        private IEnumerable<PositionOnBoard> GridPositions()
        {
            for (var row = _levelSettings.NumberOfRows - 2; row >= 0; row--)
            {
                for (var column = 0; column < _levelSettings.NumberOfColumns; column++)
                {
                    var columnPosition = _fillInverse ? _levelSettings.NumberOfColumns - 1 - column : column;
                    yield return new PositionOnBoard(row, columnPosition);
                }
            }
        }

        private void UpdateEntity(PositionOnBoard updatePosition)
        {
            if (!_grid[updatePosition].CanMove)
            {
                return;
            }

            if (!UpdateEntityVertically(updatePosition))
            {
                UpdateEntityDiagonaly(updatePosition);
            }
        }

        private bool UpdateEntityVertically(PositionOnBoard updatePosition)
        {
            var positionBelow = new PositionOnBoard(updatePosition.Row + 1, updatePosition.Column);
            var entityBelow = _grid[positionBelow];

            if (entityBelow != null) return false;

            DropEntity(updatePosition, false);
            return true;
        }

        private void UpdateEntityDiagonaly(PositionOnBoard updatePosition)
        {
            if (!UpdateEntityWithDiagonalOffset(updatePosition, -1))
            {
                UpdateEntityWithDiagonalOffset(updatePosition, 1);
            }
        }

        private bool UpdateEntityWithDiagonalOffset(PositionOnBoard updatePosition, int diagonalOffset)
        {
            var diagColumn = _fillInverse ? updatePosition.Column - diagonalOffset : updatePosition.Column + diagonalOffset;
            if (diagColumn < 0 || diagColumn >= _levelSettings.NumberOfColumns)
            {
                return false;
            }

            var diagonalEntityBellow = _grid[new PositionOnBoard(updatePosition.Row + 1, diagColumn)];
            if (diagonalEntityBellow != null)
            {
                return false;
            }

            if (HasEntityAbove(updatePosition, diagColumn))
            {
                return false;
            }

            DropEntity(updatePosition, true);
            return true;
        }

        private bool HasEntityAbove(PositionOnBoard updatePosition, int diagColumn)
        {
            var hasEntityAbove = true;

            for (var aboveRow = updatePosition.Row; aboveRow >= 0; aboveRow--)
            {
                var entityAbove = _grid[new PositionOnBoard(aboveRow, diagColumn)];

                if (entityAbove == null)
                {
                    break;
                }

                if (entityAbove.CanMove)
                {
                    break;
                }

                if (entityAbove.CanMove) continue;
                hasEntityAbove = false;
                break;
            }

            return hasEntityAbove;
        }

        private void DropEntity(PositionOnBoard updatePosition, bool dioganaly)
        {
            var oldPosition = updatePosition.Clone() as PositionOnBoard;
            var newPosition = GetNewPosition(updatePosition, dioganaly);

            var entityUpdatedEventArgs = new EntityUpdatedParam
            {
                FromPosition = oldPosition,
                ToPosition = newPosition
            };

            _grid.UpdateEntity(entityUpdatedEventArgs);

            _continueUpdate = true;
        }

        private PositionOnBoard GetNewPosition(PositionOnBoard updatePosition, bool dioganaly)
        {
            if (!dioganaly)
            {
                return new PositionOnBoard(updatePosition.Row + 1, updatePosition.Column);
            }

            return _fillInverse
                ? new PositionOnBoard(updatePosition.Row + 1, updatePosition.Column - 1)
                : new PositionOnBoard(updatePosition.Row + 1, updatePosition.Column + 1);
        }

        private void AddNewEntities()
        {
            for (var column = 0; column < _levelSettings.NumberOfColumns; column++)
            {
                var entity = _grid[new PositionOnBoard(0, column)];
                if (entity == null)
                {
                    AddNewEntity(column);
                }
            }
        }

        private void AddNewEntity(int column)
        {
            var newPositionOnBoard = new PositionOnBoard(0, column);

            var entityUpdatedEventArgs = new EntityUpdatedParam
            {
                NewCreatedEntity = _cardResolver.GetCard(),
                FromPosition = newPositionOnBoard,
                ToPosition = newPositionOnBoard
            };

            _grid.UpdateEntity(entityUpdatedEventArgs);

            _continueUpdate = true;
        }

    }
}
