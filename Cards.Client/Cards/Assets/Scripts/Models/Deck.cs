﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class Deck
    {
        private readonly IList<Card> _cards;

        private readonly Random _random = new Random();
        
        public Deck()
        {
            _cards = new List<Card>();
            AddCards();
        }

        public Card Draw()
        {
            var index = _random.Next(0, _cards.Count);
            var card = _cards[index];
            _cards.RemoveAt(index);

            return card;
        }

        public void Return(Card card)
        {
            if (card == null)
            {
                return;
            }

            _cards.Add(card);
        }

        private void AddCards()
        {
            _cards.Add(new Card(Suit.Spades, Rank.Ace));
            _cards.Add(new Card(Suit.Spades, Rank.King));
            _cards.Add(new Card(Suit.Spades, Rank.Queen));
            _cards.Add(new Card(Suit.Spades, Rank.Jack));
            _cards.Add(new Card(Suit.Spades, Rank.Ten));
            _cards.Add(new Card(Suit.Spades, Rank.Nine));
            _cards.Add(new Card(Suit.Spades, Rank.Eight));
            _cards.Add(new Card(Suit.Spades, Rank.Seven));
            _cards.Add(new Card(Suit.Spades, Rank.Six));
            _cards.Add(new Card(Suit.Spades, Rank.Five));
            _cards.Add(new Card(Suit.Spades, Rank.Four));
            _cards.Add(new Card(Suit.Spades, Rank.Three));
            _cards.Add(new Card(Suit.Spades, Rank.Two));

            _cards.Add(new Card(Suit.Diamonds, Rank.Ace));
            _cards.Add(new Card(Suit.Diamonds, Rank.King));
            _cards.Add(new Card(Suit.Diamonds, Rank.Queen));
            _cards.Add(new Card(Suit.Diamonds, Rank.Jack));
            _cards.Add(new Card(Suit.Diamonds, Rank.Ten));
            _cards.Add(new Card(Suit.Diamonds, Rank.Nine));
            _cards.Add(new Card(Suit.Diamonds, Rank.Eight));
            _cards.Add(new Card(Suit.Diamonds, Rank.Seven));
            _cards.Add(new Card(Suit.Diamonds, Rank.Six));
            _cards.Add(new Card(Suit.Diamonds, Rank.Five));
            _cards.Add(new Card(Suit.Diamonds, Rank.Four));
            _cards.Add(new Card(Suit.Diamonds, Rank.Three));
            _cards.Add(new Card(Suit.Diamonds, Rank.Two));

            _cards.Add(new Card(Suit.Clubs, Rank.Ace));
            _cards.Add(new Card(Suit.Clubs, Rank.King));
            _cards.Add(new Card(Suit.Clubs, Rank.Queen));
            _cards.Add(new Card(Suit.Clubs, Rank.Jack));
            _cards.Add(new Card(Suit.Clubs, Rank.Ten));
            _cards.Add(new Card(Suit.Clubs, Rank.Nine));
            _cards.Add(new Card(Suit.Clubs, Rank.Eight));
            _cards.Add(new Card(Suit.Clubs, Rank.Seven));
            _cards.Add(new Card(Suit.Clubs, Rank.Six));
            _cards.Add(new Card(Suit.Clubs, Rank.Five));
            _cards.Add(new Card(Suit.Clubs, Rank.Four));
            _cards.Add(new Card(Suit.Clubs, Rank.Three));
            _cards.Add(new Card(Suit.Clubs, Rank.Two));
        }
    }
}
