﻿
namespace Assets.Scripts
{
    public class Card : IEntity
    {
        public Suit Suit { get; private set; }
        public Rank Rank { get; private set; }

        public bool CanMove { get { return true; } }

        public Card(Suit suit, Rank rank)
        {
            Suit = suit;
            Rank = rank;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Card)) return false;

            return ((Card)obj).Rank == Rank && ((Card)obj).Suit == Suit;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Suit.GetHashCode() * 397 + Rank.GetHashCode() * 391;
            }
        }

        public override string ToString()
        {
            return Suit + "." + Rank;
        }
    }
}
