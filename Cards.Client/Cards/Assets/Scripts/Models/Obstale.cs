﻿
namespace Assets.Scripts
{
    public class Obstacle : IEntity
    {
        public Dice Dice { get; set; }

        public bool CanMove { get { return false; } }

        public Obstacle(Dice dice)
        {
            Dice = dice;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Obstacle)) return false;

            return ((Obstacle)obj).Dice == Dice;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Dice.GetHashCode() * 371;
            }
        }

        public override string ToString()
        {
            return "Dice." + Dice;
        }
    }
}
