﻿
namespace Assets.Scripts
{
    public interface IGrid
    {
        IGameEntity this[PositionOnBoard positionOnBoard] { get; }

        void Init();
        void RemoveEntity(PositionOnBoard positionOnBoard);
        void UpdateEntity(EntityUpdatedParam e);

        bool IsLegalSwipe(PositionOnBoard fromPosition, PositionOnBoard toPosition);
        void SwipeCards(PositionOnBoard fromPosition, PositionOnBoard toPosition);
    }
}
