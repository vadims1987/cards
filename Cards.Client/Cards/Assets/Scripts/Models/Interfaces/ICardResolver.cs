﻿namespace Assets.Scripts
{
    public interface ICardResolver
    {
        Card GetCard();
        void ReturnCard(Card card);
    }
}
