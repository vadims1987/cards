﻿
namespace Assets.Scripts
{
    public interface ICard
    {
        Suit Suit { get; }
        Rank Rank { get; }
    }
}
