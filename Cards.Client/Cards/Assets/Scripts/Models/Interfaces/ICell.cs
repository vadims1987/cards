﻿
namespace Assets.Scripts
{
    public interface ICell
    {
        PositionOnBoard PositionOnBoard { get;}
        IGameEntity Entity { get; set; }
    }
}
