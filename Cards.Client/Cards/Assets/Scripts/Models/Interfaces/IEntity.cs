﻿
namespace Assets.Scripts
{
    public interface IEntity
    {
        bool CanMove { get; }
    }
}
