﻿
namespace Assets.Scripts
{
    public interface IGameCard
    {
        Card Card { get; }
    }
}
