﻿using UnityEngine;

namespace Assets.Scripts
{
    public interface IGameEntity : IEntity
    {
        void Init(IEntity entity, Sprite sprite);
        void UpdatePosition(PositionOnBoard newPosition);
        void Clear();
    }
}
