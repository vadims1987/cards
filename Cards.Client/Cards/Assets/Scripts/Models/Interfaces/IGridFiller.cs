﻿
namespace Assets.Scripts
{
    public interface IGridFiller
    {
        bool FillGrid(IGrid grid);
    }
}
