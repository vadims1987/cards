﻿
namespace Assets.Scripts
{
    public interface IEntityOnBoard
    {
        IEntity Entity { get; }
        PositionOnBoard PositionOnBoard { get; set; }
    }
}
