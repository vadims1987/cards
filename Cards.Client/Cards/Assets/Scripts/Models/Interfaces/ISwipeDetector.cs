﻿
namespace Assets.Scripts
{
    public interface ISwipeDetector
    {
        void TileEnterDetected(PositionOnBoard positionOnBoard);
        void TilePressDetected(PositionOnBoard positionOnBoard);
        void TileReleaseDetected();
    }
}
