﻿
namespace Assets.Scripts
{
    public interface IGameEntitySpawner
    {
        IGameEntity CreateGameEntity(IEntity entity, PositionOnBoard position);
    }
}
