﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public class LevelSettingsMap : ILevelMap<ILevelSettings>
    {
        private static readonly IDictionary<Level, ILevelSettings> _levelSettingsMap = new Dictionary<Level, ILevelSettings>();

        private readonly ILevelMap<ILevelObjectives> _levelObjectivesMap;

        public LevelSettingsMap(ILevelMap<ILevelObjectives> levelObjectivesMap)
        {
            _levelObjectivesMap = levelObjectivesMap;
        }

        public void Setup(IEnumerable<ILevelSettings> source)
        {
            var levelNumber = 1;

            foreach (var levelSettings in source)
            {
                var level = new Level { Number = levelNumber };

                levelSettings.LevelObjectives = _levelObjectivesMap[level];
                _levelSettingsMap[level] = levelSettings;

                levelNumber++;
            }
        }

        public ILevelSettings this[Level level]
        {
            get { return _levelSettingsMap[level]; }
        }
    }
}