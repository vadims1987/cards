﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public class LevelObjectivesMap : ILevelMap<ILevelObjectives>
    {
        private readonly IDictionary<Level, ILevelObjectives> _levelObjectivesMap = new Dictionary<Level, ILevelObjectives>();

        public void Setup(IEnumerable<ILevelObjectives> source)
        {
            var levelNumber = 1;

            foreach (var levelObjetives in source)
            {
                var level = new Level { Number = levelNumber };
                _levelObjectivesMap[level] = levelObjetives;

                levelNumber++;
            }
        }

        public ILevelObjectives this[Level level]
        {
            get { return _levelObjectivesMap[level]; }
        }
    }
}