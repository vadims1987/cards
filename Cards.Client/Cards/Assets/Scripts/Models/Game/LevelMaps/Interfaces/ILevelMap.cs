﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public interface ILevelMap<T>
    {
        void Setup(IEnumerable<T> source);
        T this[Level level] { get; }
    }
}
