﻿
using System.Collections.Generic;

namespace Assets.Scripts
{
    public static class LegalHandsPointsMap
    {
        private static readonly Dictionary<LegalHand, int> PointsMap = new Dictionary<LegalHand, int>();

        static LegalHandsPointsMap()
        {
            PointsMap.Add(LegalHand.None, 0);
            PointsMap.Add(LegalHand.Pair, 20);
            PointsMap.Add(LegalHand.TwoPairs, 30);
            PointsMap.Add(LegalHand.ThreeOfKind, 40);
            PointsMap.Add(LegalHand.Straight, 50);
            PointsMap.Add(LegalHand.Flush, 60);
            PointsMap.Add(LegalHand.FullHouse, 70);
            PointsMap.Add(LegalHand.FourOfKind, 80);
            PointsMap.Add(LegalHand.StraightFlush, 90);
            PointsMap.Add(LegalHand.Royal, 100);
        }

        public static int GetPoints(LegalHand legalHand)
        {
            return PointsMap[legalHand];
        }
    }
}
