﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts
{
    public class LevelManager : ILevelManager
    {
        private readonly ILegalHandsFinder _legalHandsFinder;

        private readonly IGridFiller _gridFiller;

        private readonly IGrid _grid;

        private readonly ITurnManager _turnManager;

        public LevelStatus LevelStatus
        {
            get
            {
                if (_turnManager.IsGameWon())
                {
                    return LevelStatus.Won;
                }

                return _turnManager.IsGameLost() ? LevelStatus.Lost : LevelStatus.InProgress;
            }
        }

        [Inject]
        public LevelManager(ILegalHandsFinder legalHandsFinder, IGrid grid, IGridFiller gridFiller,
            ITurnManager turnManager)
        {

            _legalHandsFinder = legalHandsFinder;
            _grid = grid;
            _gridFiller = gridFiller;
            _turnManager = turnManager;
        }

        public IEnumerator StartGame()
        {
            _grid.Init();

            yield return FillBoard();
        }

        public IEnumerator PlayTurn(PositionOnBoard fromPosition, PositionOnBoard toPosition)
        {
            yield return MoveCards(fromPosition, toPosition);

            var needsClearing = FindLegalHands();
            if (!needsClearing)
            {
                yield return ReturnCards(fromPosition, toPosition);
            }
            else
            {
                _turnManager.UpdateTurn();

                yield return new WaitForSeconds(AnimationConstants.ClearAnimationTime);
                yield return FillBoard();
            }
        }

        private IEnumerator MoveCards(PositionOnBoard fromPosition, PositionOnBoard toPosition)
        {
            _grid.SwipeCards(fromPosition, toPosition);
            yield return new WaitForSeconds(AnimationConstants.MoveAnimationTime);
        }

        private IEnumerator ReturnCards(PositionOnBoard fromPosition, PositionOnBoard toPosition)
        {
            _grid.SwipeCards(toPosition, fromPosition);
            yield return new WaitForSeconds(AnimationConstants.MoveAnimationTime);
        }

        public IEnumerator FillBoard()
        {
            var needsRefill = true;

            while (needsRefill)
            {
                yield return new WaitForSeconds(AnimationConstants.MoveAnimationTime);

                while (_gridFiller.FillGrid(_grid))
                {
                    yield return new WaitForSeconds(AnimationConstants.MoveAnimationTime);
                }

                needsRefill = FindLegalHands();
                if (needsRefill)
                {
                    yield return new WaitForSeconds(AnimationConstants.ClearAnimationTime);
                }
            }
        }

        private bool FindLegalHands()
        {
            var foundLegalHands = _legalHandsFinder.FindLegalHands(_grid);

            _turnManager.UpdateObjectives(foundLegalHands);

            ClearLegalHands(foundLegalHands);

            return foundLegalHands.Any();
        }

        private void ClearLegalHands(IEnumerable<FoundLegalHand> foundLegalHands)
        {
            var entitiesToRemove = GetEntitiesToRemove(foundLegalHands);
            DestroyGameCards(entitiesToRemove);
        }

        private static IEnumerable<IEntityOnBoard> GetEntitiesToRemove(IEnumerable<FoundLegalHand> foundLegalHands)
        {
            var entitiesToRemove = (from foundLegalHand in foundLegalHands
                                    from IEntityOnBoard entity in foundLegalHand.LegalHandOnBoard
                                    select entity).ToList();

            return entitiesToRemove;
        }

        private void DestroyGameCards(IEnumerable<IEntityOnBoard> entitiesToRemove)
        {
            var poistionsToRemove = entitiesToRemove
                .Select(entity => (PositionOnBoard)entity.PositionOnBoard.Clone()).ToList();

            foreach (var position in poistionsToRemove)
            {
                var entity = _grid[position];

                if (entity == null) continue;

                _grid.RemoveEntity(position);
            }
        }
    }
}
