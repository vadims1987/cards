﻿using Zenject;

namespace Assets.Scripts
{
    public class LevelSettingsHandler : ILevelSettingsHandler
    {
        public Level CurrentLevel { get; set; }

        [Inject]
        private ILevelMap<ILevelSettings> LevelSettingsMap { get; set; }

        public LevelSettingsHandler([InjectOptional] Level level)
        {
            CurrentLevel = level ?? new Level {Number = 1};
        }

        public ILevelSettings LevelSettings
        {
            get { return LevelSettingsMap[CurrentLevel]; }
        }
    }
}
