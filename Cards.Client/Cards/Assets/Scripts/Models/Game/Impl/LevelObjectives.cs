using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts
{
    public class LevelObjectives : ILevelObjectives
    {
        private TargetScore _targetScore = new TargetScore();

        public TargetScore TargetScore
        {
            get { return _targetScore; }
            set { _targetScore = value; }
        }

        private IEnumerable<TargetLegalHand> _targetLegalHands = new List<TargetLegalHand>();

        public IEnumerable<TargetLegalHand> TargetLegalHands
        {
            get { return _targetLegalHands; }
            set { _targetLegalHands = value; }
        }

        public void UpdateLegalHandObjective(LegalHand legalHand)
        {
            var objective = _targetLegalHands.First(targetLegalHand => targetLegalHand.LegalHand == legalHand);
            objective.CurrentCount++;
        }

        public bool IsComplete()
        {
            return TargetScore.IsCompleted 
                && TargetLegalHands.All(targetLegalHand => targetLegalHand.IsCompleted);
        }
    }
}