﻿using System.Collections.Generic;
using Zenject;

namespace Assets.Scripts
{
    public class TurnManager : ITurnManager
    {
        private int _turnsLeft;

        private readonly TurnUpdatedSignal _turnUpdatedSignal;

        private readonly ILevelSettings _levelSettings;

        private readonly IObjectivesManager _objectivesManager;

        [Inject]
        private TurnManager(TurnUpdatedSignal turnUpdatedSignal, 
            ILevelSettings levelSettings, 
            IObjectivesManager objectivesManager)
        {
            _turnUpdatedSignal = turnUpdatedSignal;
            _levelSettings = levelSettings;
            _objectivesManager = objectivesManager;

            SetIntialTurnStatus();
        }

        private void SetIntialTurnStatus()
        {
            _turnsLeft = _levelSettings.MaxNumberOfTurns;

            SignalTurnUpdated();
        }

        public void UpdateTurn()
        {
            _turnsLeft--;

            SignalTurnUpdated();
        }

        private void SignalTurnUpdated()
        {
            _turnUpdatedSignal.Fire(new TurnUpdatedSignalArgs
            {
                TurnsLeft = _turnsLeft
            });
        }

        public void UpdateObjectives(IEnumerable<FoundLegalHand> foundLegalHands)
        {
            _objectivesManager.UpdateObjectives(foundLegalHands);
        }

        public bool IsGameLost()
        {
            return _turnsLeft == 0 && !_objectivesManager.IsGameWon();
        }

        public bool IsGameWon()
        {
            return _objectivesManager.IsGameWon();
        }
    }
}
