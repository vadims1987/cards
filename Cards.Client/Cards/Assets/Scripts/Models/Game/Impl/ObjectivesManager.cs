using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace Assets.Scripts
{
    public class ObjectivesManager : IObjectivesManager
    {
        private readonly ILevelObjectives _levelObjectives;

        private readonly ObjectivesUpdatedSignal _objectivesUpdatedSignal;

        [Inject]
        public ObjectivesManager(ILevelSettings levelSettings,
            ObjectivesUpdatedSignal objectivesUpdatedSignal)
        {
            _objectivesUpdatedSignal = objectivesUpdatedSignal;
            _levelObjectives = levelSettings.LevelObjectives;

            FireObjectivesUpdated();
        }

        public bool IsGameWon()
        {
            return _levelObjectives.IsComplete();
        }

        public void UpdateObjectives(IEnumerable<FoundLegalHand> foundLegalHands)
        {
            UpdateScore(foundLegalHands);
            UpdateLegalHands(foundLegalHands);

            FireObjectivesUpdated();
        }

        private void FireObjectivesUpdated()
        {
            _objectivesUpdatedSignal.Fire(new ObjectivesUpdatedSignalArgs
            {
                LevelObjectives = _levelObjectives
            });
        }

        private void UpdateScore(IEnumerable<FoundLegalHand> foundLegalHands)
        {
            var addedScore = GetAddedScore(foundLegalHands);
            _levelObjectives.TargetScore.CurrentCount += addedScore;
        }

        private static int GetAddedScore(IEnumerable<FoundLegalHand> foundLegalHands)
        {
            return foundLegalHands.Sum(foundLegalHand => LegalHandsPointsMap.GetPoints(foundLegalHand.LegalHand));
        }

        private void UpdateLegalHands(IEnumerable<FoundLegalHand> foundLegalHands)
        {
            foreach (var foundLegalHand in foundLegalHands)
            {
                if (IsLegalHandObjective(foundLegalHand.LegalHand))
                {
                    _levelObjectives.UpdateLegalHandObjective(foundLegalHand.LegalHand);
                }
            }
        }

        private bool IsLegalHandObjective(LegalHand legalHand)
        {
            return _levelObjectives.TargetLegalHands.Any(targetLegalHands => targetLegalHands.LegalHand == legalHand);
        }
    }
}