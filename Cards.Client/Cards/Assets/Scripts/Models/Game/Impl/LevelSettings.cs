﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts
{
    public class LevelSettings : ILevelSettings
    {
        private int _numberOfRows = 7;

        public int NumberOfRows
        {
            get { return _numberOfRows; }
            set { _numberOfRows = value; }
        }

        private int _numberOfColumns = 5;

        public int NumberOfColumns
        {
            get { return _numberOfColumns; }
            set { _numberOfColumns = value; }
        }

        private int _maxNumberOfTurns = 50;

        public int MaxNumberOfTurns
        {
            get { return _maxNumberOfTurns; }
            set { _maxNumberOfTurns = value; }
        }

        public IList<ObstacleOnBoard> Obstacles { get; set; }

        public IList<CardOnBoard> PredefinedCards { get; set; }

        public ILevelObjectives LevelObjectives { get; set; }

        public LevelSettings()
        {
            Obstacles = new List<ObstacleOnBoard>();

            PredefinedCards = new List<CardOnBoard>();

            LevelObjectives = new LevelObjectives();
        }

        public CardOnBoard GetCardOnBoard(PositionOnBoard position)
        {
            return PredefinedCards.FirstOrDefault(cardOnBoard => Equals(cardOnBoard.PositionOnBoard, position));
        }
    }
}
