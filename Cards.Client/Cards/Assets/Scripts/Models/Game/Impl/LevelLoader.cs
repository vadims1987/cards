﻿
using UnityEngine.SceneManagement;
using Zenject;

namespace Assets.Scripts
{
    public class LevelLoader : ILevelLoader
    {
        private readonly ZenjectSceneLoader _sceneLoader;

        public LevelLoader(ZenjectSceneLoader sceneLoader)
        {
            _sceneLoader = sceneLoader;
        }

        public void LoadLevel(Level level)
        {
            _sceneLoader.LoadScene(Scenes.LevelScene, LoadSceneMode.Single, container =>
            {
                container.BindInstance(level).WhenInjectedInto<LevelSettingsHandler>();
            });
        }

        public void LoadMainScene()
        {
            _sceneLoader.LoadScene(Scenes.MainScene, LoadSceneMode.Single);
        }
    }
}
