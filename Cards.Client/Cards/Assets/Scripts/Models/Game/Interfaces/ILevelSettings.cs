﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public interface ILevelSettings
    {
        IList<ObstacleOnBoard> Obstacles { get; set; }

        ILevelObjectives LevelObjectives{ get; set; }

        int NumberOfRows { get; set; }

        int NumberOfColumns { get; set; }

        int MaxNumberOfTurns { get; set; }

        IList<CardOnBoard> PredefinedCards { get; set; }
        CardOnBoard GetCardOnBoard(PositionOnBoard position);
    }
}
