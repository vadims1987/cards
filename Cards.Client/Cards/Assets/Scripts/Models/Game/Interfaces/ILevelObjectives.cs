﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public interface ILevelObjectives
    {
        TargetScore TargetScore { get; set; }
        IEnumerable<TargetLegalHand> TargetLegalHands { get; set; }

        void UpdateLegalHandObjective(LegalHand legalHand);
        bool IsComplete();
    }
}
