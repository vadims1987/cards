﻿
namespace Assets.Scripts
{
    interface ILevelLoader
    {
        void LoadLevel(Level level);
        void LoadMainScene();
    }
}
