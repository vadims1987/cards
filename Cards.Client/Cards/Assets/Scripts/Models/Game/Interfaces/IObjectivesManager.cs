﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public interface IObjectivesManager
    {
        bool IsGameWon();
        void UpdateObjectives(IEnumerable<FoundLegalHand> foundLegalHands);
    }
}
