﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public interface ITurnManager
    {
        bool IsGameLost();
        bool IsGameWon();

        void UpdateObjectives(IEnumerable<FoundLegalHand> foundLegalHands);
        void UpdateTurn();
    }
}
