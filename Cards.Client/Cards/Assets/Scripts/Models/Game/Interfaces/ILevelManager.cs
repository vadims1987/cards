﻿
using System.Collections;

namespace Assets.Scripts
{
    public interface ILevelManager
    {
        IEnumerator PlayTurn(PositionOnBoard fromPosition, PositionOnBoard toPosition);
        IEnumerator StartGame();
        LevelStatus LevelStatus { get; }
    }
}
