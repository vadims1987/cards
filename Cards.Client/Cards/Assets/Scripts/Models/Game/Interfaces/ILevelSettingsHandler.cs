﻿
namespace Assets.Scripts
{
    public interface ILevelSettingsHandler
    {
        Level CurrentLevel { get; set; }
        ILevelSettings LevelSettings { get; }
    }
}
