﻿
namespace Assets.Scripts
{
    public interface ITestSettingsHandler
    {
        Level CurrentLevel { get; set; }
        ILevelSettings LevelSettings { get; }
    }
}
