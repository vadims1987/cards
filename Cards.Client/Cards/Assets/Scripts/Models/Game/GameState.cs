﻿
namespace Assets.Scripts.Models.Game
{
    public class GameState
    {
        public GameState()
        {
            CurrentLevel = 1;
        }

        public int CurrentLevel { get; set; }
    }
}
