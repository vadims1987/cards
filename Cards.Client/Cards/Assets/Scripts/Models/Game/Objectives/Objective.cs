﻿
namespace Assets.Scripts
{
    public abstract class Objective
    {
        public int TargetCount;

        private int _currentCount;

        public int CurrentCount
        {
            get { return _currentCount; }
            set
            {
                if (IsCompleted)
                {
                    return;
                }

                _currentCount = value;
                if (_currentCount >= TargetCount)
                {
                    IsCompleted = true;
                }
            }
        }

        public bool IsCompleted { get; set; }
    }
}
