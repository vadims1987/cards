﻿namespace Assets.Scripts
{
    public enum LevelStatus
    {
        InProgress,
        Won,
        Lost
    }
}
