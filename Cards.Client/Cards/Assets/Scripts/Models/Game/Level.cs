﻿
namespace Assets.Scripts
{
    public class Level
    {
        public int Number { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is Level)) return false;

            return ((Level)obj).Number == Number;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Number.GetHashCode() * 397;
            }
        }
    }
}
