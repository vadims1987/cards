﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class SpritesMap
    {
        private readonly IDictionary<string, Sprite> _spritesMap = new Dictionary<string, Sprite>();

        public SpritesMap()
        {
            Initialize();
        }

        public void Initialize()
        {
            var sprites = Resources.LoadAll<Sprite>("Sprites");

            foreach (var sprite in sprites)
            {
                _spritesMap.Add(sprite.name, sprite);
            }
        }

        public Sprite this[IEntity entity]
        {
            get
            {
                return _spritesMap[entity.ToString()];
            }
        }
    }
}
