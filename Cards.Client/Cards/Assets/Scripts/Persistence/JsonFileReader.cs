﻿using System.IO;
using UnityEngine;

namespace Assets.Scripts.Persistence
{
    public class JsonFileReader<TSource, TTarget>
    {
        private readonly IMapper<TSource, TTarget> _mapper;

        public JsonFileReader(IMapper<TSource, TTarget> mapper)
        {
            _mapper = mapper;
        }

        public TTarget ReadAsJson(string fileName)
        {
            var file = Resources.Load<TextAsset>(fileName);

            if (file == null)
            {
                throw new FileNotFoundException();
            }

            var json = JsonUtility.FromJson<TSource>(file.text);

            var result = _mapper.Map(json);

            return result;
        }
    }
}
