﻿
using System;

namespace Assets.Scripts.Persistence
{
    [Serializable]
    public class CardDTO
    {
        public Rank rank;
        public Suit suit;
        public int x;
        public int y;
    }
}
