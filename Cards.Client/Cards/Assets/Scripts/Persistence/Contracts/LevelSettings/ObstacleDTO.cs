﻿
using System;

namespace Assets.Scripts.Persistence
{
    [Serializable]
    public class ObstacleDTO
    {
        public Dice dice;
        public int x;
        public int y;
    }
}
