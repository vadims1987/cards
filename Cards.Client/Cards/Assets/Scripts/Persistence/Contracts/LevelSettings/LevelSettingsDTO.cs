﻿
using System;

namespace Assets.Scripts.Persistence
{
    [Serializable]
    public class LevelSettingsDTO
    {
        public ObstacleDTO[] obstacles;
        public CardDTO[] cards;
        public int rows;
        public int columns;
    }
}
