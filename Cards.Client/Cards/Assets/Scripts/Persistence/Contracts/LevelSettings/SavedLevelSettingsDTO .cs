﻿using System;

namespace Assets.Scripts.Persistence
{
    [Serializable]
    public class SavedLevelSettingsDTO
    {
        public LevelSettingsDTO[] levelSettingsList;
    }
}
