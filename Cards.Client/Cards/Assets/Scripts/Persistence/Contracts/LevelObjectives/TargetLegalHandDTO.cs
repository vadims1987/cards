﻿using System;

namespace Assets.Scripts.Persistence
{
    [Serializable]
    public class TargetLegalHandDTO
    {
        public LegalHand legalHand;
        public int targetCount;
    }
}
