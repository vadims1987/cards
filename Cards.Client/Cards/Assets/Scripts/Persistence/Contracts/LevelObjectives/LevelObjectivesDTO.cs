﻿using System;

namespace Assets.Scripts.Persistence
{
    [Serializable]
    public class LevelObjectivesDTO
    {
        public int targetScore;
        public TargetLegalHandDTO[] targetLegalHands;
    }
}
