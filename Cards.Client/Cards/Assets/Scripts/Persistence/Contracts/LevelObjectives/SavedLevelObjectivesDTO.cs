﻿using System;

namespace Assets.Scripts.Persistence
{
    [Serializable]
    public class SavedLevelObjectivesDTO
    {
        public LevelObjectivesDTO[] levelObjectivesList;
    }
}
