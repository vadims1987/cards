﻿
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Persistence
{
    public class LevelSettingsMapper : IMapper<SavedLevelSettingsDTO, IEnumerable<ILevelSettings>>
    {
        public IEnumerable<ILevelSettings> Map(SavedLevelSettingsDTO source)
        {
            return source.levelSettingsList.Select(MapLevelObjectives).ToList();
        }

        private ILevelSettings MapLevelObjectives(LevelSettingsDTO source)
        {
            var levelSettings = new LevelSettings();

            SetObstacles(levelSettings, source);
            SetCards(levelSettings, source);
            SetGridSize(levelSettings, source);

            return levelSettings;
        }

        private void SetObstacles(ILevelSettings levelSettings, LevelSettingsDTO source)
        {
            if (source.obstacles == null)
            {
                return;
            }

            foreach (var obstacle in source.obstacles)
            {
                levelSettings.Obstacles.Add(new ObstacleOnBoard
                {
                    Obstacle = new Obstacle(obstacle.dice),
                    PositionOnBoard = new PositionOnBoard(obstacle.x, obstacle.y)
                });
            }
        }

        private void SetCards(ILevelSettings levelSettings, LevelSettingsDTO source)
        {
            if (source.cards == null)
            {
                return;
            }

            foreach (var card in source.cards)
            {
                levelSettings.PredefinedCards.Add(new CardOnBoard
                {
                    Card = new Card(card.suit, card.rank),
                    PositionOnBoard = new PositionOnBoard(card.x, card.y)
                });
            }
        }

        private void SetGridSize(ILevelSettings levelSettings, LevelSettingsDTO source)
        {
            if (source.rows != 0)
            {
                levelSettings.NumberOfRows = source.rows;
            }

            if (source.columns != 0)
            {
                levelSettings.NumberOfColumns = source.columns;
            }
        }
    }
}
