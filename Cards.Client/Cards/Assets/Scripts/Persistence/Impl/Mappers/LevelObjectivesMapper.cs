﻿
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Persistence
{
    public class LevelObjectivesMapper : IMapper<SavedLevelObjectivesDTO, IEnumerable<ILevelObjectives>>
    {
        public IEnumerable<ILevelObjectives> Map(SavedLevelObjectivesDTO source)
        {
            return source.levelObjectivesList.Select(MapLevelObjectives).ToList();
        }

        private ILevelObjectives MapLevelObjectives(LevelObjectivesDTO source)
        {
            var targetScore = new TargetScore
            {
                TargetCount = source.targetScore > 0 ? source.targetScore : 0,
                IsCompleted = source.targetScore <= 0
            };

            var levelObjetives = new LevelObjectives
            {
                TargetScore = targetScore
            };

            if (source.targetLegalHands == null) return levelObjetives;

            var tagetLegalHands = CreateTargetLegalHands(source.targetLegalHands);
            levelObjetives.TargetLegalHands = tagetLegalHands;

            return levelObjetives;
        }

        private IEnumerable<TargetLegalHand> CreateTargetLegalHands(IEnumerable<TargetLegalHandDTO> targetLegalHandDtos)
        {
            return targetLegalHandDtos.Select(targetLegalHandDto => new TargetLegalHand
            {
                LegalHand = targetLegalHandDto.legalHand,
                TargetCount = targetLegalHandDto.targetCount
            }).ToList();
        }
    }
}
