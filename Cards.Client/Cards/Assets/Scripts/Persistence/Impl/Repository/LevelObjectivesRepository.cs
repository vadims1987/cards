﻿using System.Collections.Generic;

namespace Assets.Scripts.Persistence
{
    public class LevelObjectivesRepository : IRepository<IEnumerable<ILevelObjectives>>
    {
        private readonly IMapper<SavedLevelObjectivesDTO, IEnumerable<ILevelObjectives>> _mapper;

        public LevelObjectivesRepository(IMapper<SavedLevelObjectivesDTO, IEnumerable<ILevelObjectives>> mapper)
        {
            _mapper = mapper;
        }

        public IEnumerable<ILevelObjectives> Load()
        {
            var jsonFleReader = new JsonFileReader<SavedLevelObjectivesDTO, IEnumerable<ILevelObjectives>>(_mapper);

            var levelObjectives = jsonFleReader.ReadAsJson("Settings/LevelObjectives");
            return levelObjectives;
        }
    }
}
