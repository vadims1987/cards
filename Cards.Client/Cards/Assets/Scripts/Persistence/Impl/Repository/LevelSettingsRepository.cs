﻿using System.Collections.Generic;
using Zenject;

namespace Assets.Scripts.Persistence
{
    public class LevelSettingsRepository : IRepository<IEnumerable<ILevelSettings>>
    {
        [Inject]
        private readonly IMapper<SavedLevelSettingsDTO, IEnumerable<ILevelSettings>> _mapper;

        public LevelSettingsRepository(IMapper<SavedLevelSettingsDTO, IEnumerable<ILevelSettings>> mapper)
        {
            _mapper = mapper;
        }

        public IEnumerable<ILevelSettings> Load()
        {
            var jsonFleReader = new JsonFileReader<SavedLevelSettingsDTO, IEnumerable<ILevelSettings>>(_mapper);

            var levelSettings = jsonFleReader.ReadAsJson("Settings/LevelSettings");
            return levelSettings;
        }
    }
}
