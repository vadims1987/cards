﻿
namespace Assets.Scripts.Persistence
{
    public interface IMapper<in TSource, out TTarget>
    {
        TTarget Map(TSource source);
    }
}
