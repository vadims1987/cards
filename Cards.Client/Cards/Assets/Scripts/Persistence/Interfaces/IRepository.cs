﻿
namespace Assets.Scripts.Persistence
{
    public interface IRepository<out T>
    {
        T Load();
    }
}
