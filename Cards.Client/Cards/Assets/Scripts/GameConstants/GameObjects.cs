﻿
using UnityEngine;

namespace Assets.Scripts
{
    public class GameObjects
    {
        public static GameObject Grid
        {
            get { return GameObject.FindWithTag("Grid"); }
        }

        public static Object CardPrefab = Resources.Load("Prefabs/GameCard");

        public static Object ObstaclePrefab = Resources.Load("Prefabs/Obstacle");

        public static Object EmptyTilePrefab = Resources.Load("Prefabs/EmptyTile");
    }
}
