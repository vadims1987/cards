﻿
namespace Assets.Scripts
{
    public class AnimationConstants
    {
        public const float MoveAnimationTime = 0.1f;

        public const float ClearAnimationTime = 0.3f;
    }
}
