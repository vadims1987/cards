﻿using Zenject;

namespace Assets.Scripts
{
    public class GameLostSignalArgs
    {
    }

    public class GameLostSignal : Signal<GameLostSignalArgs, GameLostSignal>
    {
    }
}
