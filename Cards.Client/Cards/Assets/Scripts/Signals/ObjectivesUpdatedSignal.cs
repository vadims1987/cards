﻿using Zenject;

namespace Assets.Scripts
{
    public class ObjectivesUpdatedSignalArgs
    {
        public ILevelObjectives LevelObjectives { get; set; }
    }

    public class ObjectivesUpdatedSignal : Signal<ObjectivesUpdatedSignalArgs, ObjectivesUpdatedSignal>
    {
    }
}
