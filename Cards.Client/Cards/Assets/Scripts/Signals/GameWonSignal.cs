﻿using Zenject;

namespace Assets.Scripts
{
    public class GameWonSignalArgs
    {
    }

    public class GameWonSignal : Signal<GameWonSignalArgs, GameWonSignal>
    {
    }
}
