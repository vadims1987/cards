﻿using Zenject;

namespace Assets.Scripts
{
    public class CardSwipedSignalArgs
    {
        public PositionOnBoard FromPosition { get; set; }
        public PositionOnBoard ToPosition { get; set; }
    }

    public class CardSwipedSignal : Signal<CardSwipedSignalArgs, CardSwipedSignal>
    {
    }
}
