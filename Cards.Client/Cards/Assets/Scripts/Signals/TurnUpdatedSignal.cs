﻿using Zenject;

namespace Assets.Scripts
{
    public class TurnUpdatedSignalArgs
    {
        public int TurnsLeft { get; set; }
    }

    public class TurnUpdatedSignal : Signal<TurnUpdatedSignalArgs, TurnUpdatedSignal>
    {
    }
}
