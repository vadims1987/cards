﻿namespace Assets.Scripts
{
    public class TestCardResolver : ICardResolver
    {
        private readonly Deck _deck = new Deck();

        private readonly ILevelSettings _levelSettings;

        private int _lastDrawnRow;

        private int _lastDrawnColumn;

        private bool _hasFinishedPredefinedCards;

        private bool _hasStartedDrawing;

        public TestCardResolver(ILevelSettings levelSettings)
        {
            _levelSettings = levelSettings;
        }

        public Card GetCard()
        {
            if (!_hasStartedDrawing)
            {
                _lastDrawnRow = _levelSettings.NumberOfRows - 1;
                _hasStartedDrawing = true;
            }

            if (_hasFinishedPredefinedCards)
            {
                return _deck.Draw();
            }

            var position = new PositionOnBoard(_lastDrawnRow, _lastDrawnColumn);
            var cardToReturn = _levelSettings.GetCardOnBoard(position).Card;

            SetNextCardToReturn();

            return cardToReturn;
        }

        public void ReturnCard(Card card)
        {
            if (_hasFinishedPredefinedCards)
            {
                _deck.Return(card);
            }
        }

        private void SetNextCardToReturn()
        {
            _lastDrawnColumn++;

            if (_lastDrawnColumn != _levelSettings.NumberOfColumns)
            {
                return;
            }

            _lastDrawnRow--;
            _lastDrawnColumn = 0;

            if (_lastDrawnRow == -1)
            {
                _hasFinishedPredefinedCards = true;
            }
        }
    }
}
