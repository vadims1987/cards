﻿using System.Collections.Generic;

namespace Assets.Scripts.Test
{
    public class TestSettingsHandler : ILevelSettingsHandler
    {
        public Level CurrentLevel { get; set; }

        public ILevelSettings LevelSettings { get; private set; }

        private TestSettingsHandler()
        {
            LevelSettings = new LevelSettings
            {
                NumberOfColumns = TestCards.Cards.GetLength(0),
                NumberOfRows = TestCards.Cards.GetLength(1),
                PredefinedCards = GetCardsOnBoard(TestCards.Cards)
            };
        }

        private IList<CardOnBoard> GetCardsOnBoard(Card[,] cards)
        {
            var cardsOnBoard = new List<CardOnBoard>();

            for (var i = 0; i < cards.GetLength(0); i++)
            {
                for (var j = 0; j < cards.GetLength(1); j++)
                {
                    var position = new PositionOnBoard(i, j);
                    cardsOnBoard.Add(new CardOnBoard
                    {
                        PositionOnBoard = position,
                        Card = cards[i, j]
                    });
                }
            }

            return cardsOnBoard;
        }
    }
}
