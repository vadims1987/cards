﻿namespace Assets.Scripts.Test
{
    public static class TestCards
    {
         public static readonly Card[,] Cards = {
               {new Card(Suit.Clubs, Rank.Ace), new Card(Suit.Diamonds, Rank.Ace) },
               {new Card(Suit.Clubs, Rank.King), new Card(Suit.Diamonds, Rank.King)}
            };
    }
}


