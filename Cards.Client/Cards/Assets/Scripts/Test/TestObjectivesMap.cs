﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public class TestObjectivesMap : ILevelMap<ILevelObjectives>
    {
        public void Setup(IEnumerable<ILevelObjectives> source)
        {
            
        }

        public ILevelObjectives this[Level level]
        {
            get { return new LevelObjectives();}
        }
    }
}